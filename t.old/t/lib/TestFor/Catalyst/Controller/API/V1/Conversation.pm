package TestFor::Catalyst::Controller::API::V1::Conversation;
use base qw(ZSTest::Catalyst);

use Moose;
use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller:API::V1::Conversation - Proves the boundaries of our API: Conversation

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Conversation.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/conversation> namespace.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Create

=head3 create a conversation (item)

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/subject/create

=cut

sub cat_api_v1_conversation_create : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech        = $zs->mech;
        my $betrokkene  = $zs->create_natuurlijk_persoon_ok();

        $mech->zs_login;
        my $rv          = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/conversation/create',
            {
                type                              => 'note',
                subject_id                        => 'betrokkene-natuurlijk_persoon-' . $betrokkene->id,
                medium                            => 'behandelaar',
                message                           => "Test message\nwith a break"
            }
        );

        is($rv->{result}->{type}, 'contactmoment', 'Got type "contactmoment" from API');

        ok(exists $rv->{result}->{instance}->{$_}, "Found attribute: $_") for qw/
          case_id
          email_bcc
          email_body
          email_cc
          email_recipient
          email_subject
        /;

        ok($rv->{result}->{instance}->{$_}, "Found filled attribute: $_") for qw/
          created_by
          date_created
          medium
          message
          subject_id
          type
        /;
    }, 'api/v1/controlpanel/create: create a simple controlpanel');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
