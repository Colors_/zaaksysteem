package TestFor::General::Sysin::Xential;
use base qw(ZSTest);

use TestSetup;
use JSON::XS;

use Zaaksysteem::Backend::Sysin::Modules::Xential;

sub test_spoof_mode : Tests {
    $zs->txn_ok(
        sub {
            my $interface = $zs->create_xential_interface_ok();

            my $case = $zs->create_case_ok();

            my %params = (
                case          => $case->id,
                template_uuid => $zs->generate_uuid,
                document_title =>
                    Zaaksysteem::TestUtils::generate_random_string(),
            );

            my $pt = $interface->process_trigger('spoofmode', \%params);
            my $rs = $pt->records({}, { order => '-desc' });
            is($rs->count, 1, "Got one record");

            my $record = $rs->first;
            ok(!$record->is_error, "No error");

            my $output = decode_json($record->output);
            is($output->{transaction_id}, $pt->id, "We have reference to our transaction ID");

        }, "Xential interface tests"
    );
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
