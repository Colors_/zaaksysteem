import {
    openPage,
    openPageAs
} from './../../../../functions/common/navigate';
import {
    toggleNotifications
} from './../../../../functions/intern/mainMenu';

describe('when logging in as behandelaar', () => {
    beforeAll(() => {
        openPageAs('behandelaar');
    });

    describe('and open the notifications', () => {
        beforeAll(() => {
            toggleNotifications();
        });

        it('should not have a new account notification present', () => {
            expect($('[icon-type="account-star-variant"]').isPresent()).toBe(false);
        });
    });
});

describe('when logging in as gebruikersbeheerder', () => {
    beforeAll(() => {
        openPageAs('gebruikersbeheerder');
    });

    describe('and open the notifications', () => {
        beforeAll(() => {
            toggleNotifications();
        });

        it('should have a new account notification present', () => {
            expect($('[icon-type="account-star-variant"]').isPresent()).toBe(true);
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
