import {
    openPageAs
} from './../../../functions/common/navigate';
import {
    moveWidget,
    resizeWidget,
    deleteWidget
} from './../../../functions/intern/dashboard';
import {
    resetDashboard
} from './../../../functions/intern/actionMenu';
import {
    mouseOver
} from './../../../functions/common/mouse';

describe('when logging in as Dashboard Arrange', () => {

    beforeAll(() => {

        openPageAs('dashboardarrange');
    
    });

    describe('and when moving widget caseIntake to the position of myOpenCases', () => {
    
        beforeAll(() => {
        
            moveWidget('Zaakintake', 0, -600);

            browser.refresh();
        
        });
    
        it('the widget caseIntake should be above widget myOpenCases', () => {

            $('[data-name="mine"]').element(by.xpath('..')).getCssValue('top').then((mineTop) => {

                $('[data-name="intake"]').element(by.xpath('..')).getCssValue('top').then((intakeTop) => {

                    let mineTopInt = parseInt(mineTop.replace('px', ''), 10),
                        intakeTopInt = parseInt(intakeTop.replace('px', ''), 10);

                    expect(intakeTopInt).toBeLessThan(mineTopInt);

                });

            });

        });

        it('the widget myOpenCases should be below widget caseIntake', () => {

            $('[data-name="mine"]').element(by.xpath('..')).getCssValue('top').then((mineTop) => {

                $('[data-name="intake"]').element(by.xpath('..')).getCssValue('top').then((intakeTop) => {

                    let mineTopInt = parseInt(mineTop.replace('px', ''), 10),
                        intakeTopInt = parseInt(intakeTop.replace('px', ''), 10);

                    expect(mineTopInt).toBeGreaterThan(intakeTopInt);

                });

            });

        });
    
        afterAll(() => {
        
            resetDashboard();
        
        });
    
    });

    describe('and when resizing widget myOpenCases to be less wide and more tall', () => {

        const myOpenCases = $('[data-name="mine"]').element(by.xpath('..'));
        const intake = $('[data-name="intake"]').element(by.xpath('..'));
        const favoriteCasetypes = $('[data-name="intake"]').element(by.xpath('..'));
    
        beforeAll(() => {

            mouseOver(favoriteCasetypes.$('.handle-se'));
        
            resizeWidget('Mijn openstaande zaken', 'se', -600, 300);
        
        });

        it('the widget myOpenCases should be less wide', () => {

            myOpenCases.getCssValue('width').then((mineCssWidth) => {

                intake.getCssValue('width').then((intakeCssWidth) => {

                    const mineWidth = parseInt(mineCssWidth.replace('px', ''), 10);
                    const intakeWidth = parseInt(intakeCssWidth.replace('px', ''), 10);

                    expect( mineWidth * 1.1 < intakeWidth ).toBe(true);

                });

            });

        });

        it('the widget myOpenCases should be taller', () => {

            myOpenCases.getCssValue('height').then((mineCssHeight) => {

                intake.getCssValue('height').then((intakeCssHeight) => {

                    const mineHeight = parseInt(mineCssHeight.replace('px', ''), 10);
                    const intakeHeight = parseInt(intakeCssHeight.replace('px', ''), 10);

                    expect( mineHeight / 1.1 > intakeHeight ).toBe(true);

                });

            });

        });
    
        afterAll(() => {
        
            resetDashboard();
        
        });
    
    });

    describe('and when deleting widget myOpenCases', () => {
    
        beforeAll(() => {
        
            deleteWidget('Mijn openstaande zaken');
        
        });

        it('the widget myOpenCases should not be present', () => {

            expect($('[data-name="mine"]').isPresent()).toBe(false);

        });
    
        afterAll(() => {
        
            resetDashboard();
        
        });
    
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
