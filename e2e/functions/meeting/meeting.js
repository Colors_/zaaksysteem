export const header = $('meeting-app meeting-nav');
export const tabs = $('.meeting-nav__tabs');
export const noteBarPlaceholder = $('proposal-note-bar');
export const voteBar = $('proposal-vote-bar');
export const voteButtons = $$('.vote-button');
export const voteSave = $('.proposal-vote-view__header-buttons button:nth-child(2)');
export const voteTextarea = $('textarea');
export const noteBar = $('.proposal-detail-view__notecontent');
export const noteTextarea = $('textarea');
export const noteSave = $('.proposal-note-view__header-button .mdi-check');
export const notes = $$('.proposal-detail-view__noteplaceholder');
export const proposalDetailViews = $$('proposal-detail-view');
export const proposalContainer = $('.proposal-item-table');
export const proposals = $$('meeting-app ui-view .proposal-item-table tbody tr');
export const meetings = $$('meeting-app ui-view .meeting-item-titlebar');
export const sortingButton = $('.meeting-nav__button-bar button:nth-child(2)');
export const meetingTitleBar = $$('meeting-item-titlebar');

export const openTab = tabToOpen => {
    $(`.meeting-nav__tabs a:nth-child(${tabToOpen})`).click();
};

export const toggleMeeting = meetingToOpen => {
    const meetingElementToOpen = meetingToOpen - 1;

    meetings
        .get(meetingElementToOpen)
        .click();
};

export const toggleSorting = () => {
    sortingButton.click();
};

export const getMeetingTitleBarText = meetingToGet =>
    meetings
        .get(meetingToGet - 1)
        .getText();

export const getColumnTitles = () =>
    proposalContainer
        .$$('th:not(.small):not(.medium)')
        .map(columnTitle =>
            columnTitle.getText()
        );

export const getProposalInfo = caseNumber =>
    proposals
        .filter(proposal => 
            proposal
                .getAttribute('proposal-case-number')
                .then(proposalCaseNumber =>
                    proposalCaseNumber === caseNumber
                )
        )
        .first()
        .$$('td')
        .map(proposalColumn =>
            proposalColumn.getText()
        );

export const getProposalViewValue = valueName =>
    $$('.proposal-detail-view__list .proposal-detail-view__item')
        .filter(elm =>
            elm
                .$('.proposal-detail-view__label')
                .getText()
                .then(text =>
                    text === valueName
                )
        )
        .first()
        .$('.proposal-detail-view__value')
        .getText();


/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
