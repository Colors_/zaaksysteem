// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

export default angular
  .module('zsMoveFocusTo', [])
  .directive('zsMoveFocusTo', () => {
    return {
      link(scope, element, attr) {
        const focusHandler = () => {
          document.querySelector(attr.zsMoveFocusTo).focus();
        };

        element.on('click', focusHandler);
        scope.$on('$destroy', () => {
          element.off('click', focusHandler);
        });
      },
    };
  }).name;
