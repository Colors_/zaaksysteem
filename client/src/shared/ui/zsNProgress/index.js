// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import NProgress from 'nprogress';
import shortid from 'shortid';
import '!style-loader!css-loader!nprogress/nprogress.css';

export default angular.module('zsNProgress', []).directive('zsNProgress', [
  () => {
    return {
      restrict: 'E',
      scope: {
        active: '&',
        showSpinner: '&',
      },
      bindToController: true,
      controller: [
        '$scope',
        '$element',
        function ($scope, $element) {
          let ctrl = this,
            id = shortid();

          $element.attr('progress-id', id);

          NProgress.configure({
            parent: `[progress-id="${id}"]`,
            showSpinner: ctrl.showSpinner(),
          });

          $scope.$watch(
            () => ctrl.active(),
            (isActive, wasActive) => {
              if (isActive) {
                NProgress.start();
              } else if (wasActive) {
                NProgress.done();
              }
            }
          );
        },
      ],
      controllerAs: 'zsNProgress',
    };
  },
]).name;
