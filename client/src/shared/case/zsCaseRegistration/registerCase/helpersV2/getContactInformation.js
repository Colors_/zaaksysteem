// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const getContactInformation = (
  { $landline, $mobile, $email },
  intakeShowContactInfo
) => {
  if (!intakeShowContactInfo) {
    return;
  }

  return {
    phone_number: $landline,
    mobile_number: $mobile,
    email: $email,
  };
};

export default getContactInformation;
