// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import resourceModule from '../api/resource';

export default angular
  .module('caseRelatedCases', [resourceModule])
  .factory('caseRelatedCases', [
    '$q',
    '$rootScope',
    'resource',
    ($q, $rootScope, resource) => {
      return {
        getRelatedCases: (caseUuid) => {
          if (!caseUuid) {
            throw new Error('No case ID provided.');
          }

          const trimString = function (string) {
            const requiresTrimming = string && string.length > 20;

            if (!requiresTrimming) return string;

            return `${string.substring(0, 20)}…`;
          };

          return resource(
            () =>
              `/api/v2/cm/case/get_case_relations?case_uuid=${caseUuid}&authorization=write&include=this_case%2Cother_case`,
            {
              scope: $rootScope,
              cache: { disabled: true },
            }
          )
            .asPromise()
            .then((response) => {
              const relations = [].concat(response);

              return relations.map((relation) => {
                const caseObj = relation.relationships.other_case.data;

                const id = caseObj.id;
                const number = caseObj.attributes.number;

                const summary = trimString(caseObj.attributes.summary);
                const summaryInParentheses = summary ? ` (${summary})` : '';
                const caseType = trimString(caseObj.attributes.casetype_title);
                const name = `${number} - ${caseType}${summaryInParentheses}`;

                return { id, number, name };
              });
            });
        },
      };
    },
  ]).name;
