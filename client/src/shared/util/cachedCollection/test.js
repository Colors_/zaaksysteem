// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import 'angular-mocks';
import cachedCollectionModule from '.';
import zsStorageModule from './../zsStorage';

describe('cachedCollection', () => {
  let zsStorage;
  let cachedCollection;

  beforeEach(angular.mock.module(cachedCollectionModule, zsStorageModule));

  beforeEach(
    angular.mock.inject([
      'zsStorage',
      'cachedCollection',
      (...rest) => {
        [zsStorage, cachedCollection] = rest;
      },
    ])
  );

  afterEach(() => {
    zsStorage.clear();
  });

  test('should return a cachedCollection', () => {
    expect(cachedCollection.array('foo')).not.toBeUndefined();
  });

  test('should throw when name is left empty', () => {
    expect(() => {
      cachedCollection.array();
    }).toThrow();
  });

  test('should throw when collection with name already exists', () => {
    cachedCollection.array('foo');
    expect(cachedCollection.array.bind(null, 'foo')).toThrow();
  });

  test('should replace the value with the one supplied', () => {
    let initial = [1, 2, 3];
    let update = initial.concat(4, 5, 6);
    let array = cachedCollection.array('foo', initial);

    expect(array.value()).toEqual(initial);
    array.replace(update);
    expect(array.value()).not.toEqual(initial);
    expect(array.value()).toEqual(update);
  });

  test('should update its value when updated from elsewhere', () => {
    let initial = [1, 2, 3];
    let update = initial.concat(4, 5, 6);
    let key = 'foo';
    let array = cachedCollection.array(key, initial);

    expect(array.value()).toEqual(initial);
    zsStorage.set(key, update);
    expect(array.value()).toEqual(update);
  });

  test('should call the update listeners when updated', () => {
    let current = [1, 2, 3];
    let next = [2, 3, 4];
    let array = cachedCollection.array('foo', current);
    let spy = jasmine.createSpy('update');

    array.onUpdate.push(spy);
    array.replace(next);
    expect(spy).toHaveBeenCalled();
    expect(spy.calls.argsFor(0)[0]).toEqual(next);
    expect(spy.calls.argsFor(0)[1]).toEqual(current);
  });

  test('should get the value from cache', () => {
    let name = 'foo';
    let initial = [1, 2, 3];
    let array;

    zsStorage.set(name, initial);
    array = cachedCollection.array(name);
    expect(array.value()).toEqual(initial);
  });
});
