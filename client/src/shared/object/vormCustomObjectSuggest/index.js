// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormTemplateServiceModule from './../../vorm/vormTemplateService';
import vormCustomObjectSuggestDisplayModule from './vormCustomObjectSuggestDisplay';
import vormCustomObjectSuggestModelModule from './vormCustomObjectSuggestModel';

import './vorm-custom-object.scss';

export default angular
  .module('vormCustomObjectSuggest', [
    vormTemplateServiceModule,
    vormCustomObjectSuggestModelModule,
    vormCustomObjectSuggestDisplayModule,
  ])
  .run([
    'vormTemplateService',
    (vormTemplateService) => {
      vormTemplateService.registerType('custom-object-suggest', {
        control: angular.element(
          `<vorm-custom-object-suggest-model
            ng-model
            case-data="vm.caseData()"
            data-placeholder="{{vm.invokeData('placeholder')}}"
          >
          </vorm-custom-object-suggest-model>`
        ),
        display: angular.element(
          `<vorm-custom-object-suggest-display
            data-object="delegate.value"
            data-formatter="vm.templateData().display"
            data-icon="{{vm.templateData().icon}}"
            case-data="vm.caseData()"
            class="list-item-flex-text suggestion-display-item-text"
          >
          </vorm-custom-object-suggest-display>`
        ),
        defaults: {
          editMode: 'empty',
        },
      });
    },
  ]).name;
