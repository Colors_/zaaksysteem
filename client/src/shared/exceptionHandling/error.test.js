// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import ZaaksysteemException from './ZaaksysteemException';
import { apiV1Error, translateErrorToUI } from './error';

function mockResponseAPIv1() {
  return {
    data: {
      api_version: 1,
      request_id: 'foo-bar-baz',
      result: {
        type: 'exception',
        instance: {
          type: 'case/reject/incomplete_distributors',
          message: 'foo',
        },
      },
    },
  };
}

describe('apiV1Error', () => {
  const want = {
    type: 'case/reject/incomplete_distributors',
    requestID: 'foo-bar-baz',
  };
  test('Transform the error', () => {
    expect(apiV1Error(mockResponseAPIv1().data)).toMatchObject(want);
  });
});

describe('tranlateErrorToUI', () => {
  test('Mapped API v1 error', () => {
    const error = translateErrorToUI(mockResponseAPIv1());
    expect(error).toBe(
      'Er zijn geen zaakdistributeurs geconfigureerd, de zaak kan niet geweigerd worden'
    );
  });

  test('Unmapped error', () => {
    let response = mockResponseAPIv1();
    response.data.result.instance['type'] = 'foo/bar';

    const error = translateErrorToUI(response);
    expect(error).toBe(
      'Er is een fout opgetreden (foo-bar-baz), neem contact op met uw beheerder'
    );
  });

  test('Except the exception', () => {
    let response = mockResponseAPIv1();
    response.data.result['type'] = 'set';

    expect(() => {
      translateErrorToUI(response);
    }).toThrowError(ZaaksysteemException);
  });
});
