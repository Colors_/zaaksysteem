// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../shared/api/resource/composedReducer';
import vormInvokeModule from './../../../../../shared/vorm/vormInvoke';
import zsTooltipModule from './../../../../../shared/ui/zsTooltip';
import zsModalModule from './../../../../../shared/ui/zsModal';
import confidentialityOptions from './../../../../../shared/zs/case/confidentiality';
import auxiliaryRouteModule from './../../../../../shared/util/route/auxiliaryRoute';
import get from 'lodash/get';
import assign from 'lodash/assign';
import capitalize from 'lodash/capitalize';
import './styles.scss';
import {
  translateStatus,
  getRequestorTitle,
  getSubjectLabel,
  getSubjectUrl,
  getAssigneeLabel,
  isPassedDueDate,
  getLocationLabel,
  getPaymentStatusTitle,
  getPaymentStatusIcon,
  mapLink,
} from './library';

export default angular
  .module('zsCaseSummary', [
    composedReducerModule,
    vormInvokeModule,
    zsTooltipModule,
    zsModalModule,
    auxiliaryRouteModule,
  ])
  .directive('zsCaseSummary', [
    '$state',
    '$compile',
    'composedReducer',
    'dateFilter',
    'zsModal',
    'auxiliaryRouteService',
    (
      $state,
      $compile,
      composedReducer,
      dateFilter,
      zsModal,
      auxiliaryRouteService
    ) => {
      let transformDate = (date) =>
        date ? dateFilter(date, 'dd-MM-yyyy') : '-';

      return {
        restrict: 'E',
        template,
        scope: {
          case: '&',
          casetype: '&',
          canChangeConfidentiality: '&',
          onConfidentialityChange: '&',
          onSelfAssign: '&',
          isCollapsed: '&',
          canAssign: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function ($scope) {
            let ctrl = this,
              linkReducer,
              confidentialityReducer,
              confOptionsReducer;

            let openConfidentialityModal = () => {
              let scope = $scope.$new(),
                modal;

              scope.handleConfidentialityClick = (confidentiality) => {
                ctrl.onConfidentialityChange({
                  $confidentiality: confidentiality,
                });
                modal.close();
              };

              modal = zsModal({
                title: 'Vertrouwelijkheid wijzigen',
                el: $compile(
                  `
								<div class="confidentiality-warning">
									<zs-icon icon-type="alert"></zs-icon>
									Let op! Als u het vertrouwelijkheidsniveau wijzigt naar een niveau waarop u geen rechten heeft op de zaak, kunt u dit niet terugzetten.
								</div>
								<ul class="modal-confidentiality">

									<li ng-repeat="option in vm.getConfidentialityOptions()">
										<label>
											<input type="radio" ng-checked="vm.getConfidentiality()===option.name" ng-click="handleConfidentialityClick(option.name)">
											{{::option.label}}
										</label>
									</li>

								</ul>`
                )(scope),
              });

              modal.open();
            };

            linkReducer = composedReducer(
              { scope: $scope },
              ctrl.case,
              ctrl.canChangeConfidentiality,
              ctrl.isCollapsed,
              ctrl.canAssign,
              () => auxiliaryRouteService.getCurrentBase()
            ).reduce(
              (
                caseObj,
                canChangeConfidentiality,
                isCollapsed,
                canAssign,
                currentState
              ) => {
                const assignee = get(caseObj, 'instance.assignee.instance');
                const requestor = get(caseObj, 'instance.requestor.instance');
                const recipient = get(caseObj, 'instance.recipient.instance');
                const registrationDate = get(
                  caseObj,
                  'instance.date_of_registration'
                );
                const targetDate = get(caseObj, 'instance.date_target');
                const completionDate = get(
                  caseObj,
                  'instance.date_of_completion'
                );
                const casetype = get(caseObj, 'instance.casetype');
                const result = caseObj.instance.result;
                const status = caseObj.instance.status;
                const paymentStatus = get(caseObj, 'instance.payment_status');
                const location = caseObj.instance.location;
                const confidentiality = caseObj.instance.confidentiality;
                const caseClosed = status === 'resolved';

                const links = [
                  ...(caseClosed
                    ? {
                        icon: 'briefcase-check',
                        name: 'result',
                        title: 'Resultaat',
                        label:
                          get(caseObj, 'instance.result_description') ||
                          capitalize(result),
                      }
                    : []),
                  {
                    name: 'status',
                    title: 'Status',
                    label: translateStatus(status),
                    icon: ` icon-status-${status}`,
                  },
                  {
                    name: 'requestor',
                    title: getRequestorTitle(requestor),
                    label: getSubjectLabel(requestor),
                    url: getSubjectUrl(requestor),
                    openExternal: true,
                    icon: 'account',
                    type: 'link',
                  },
                  ...(recipient
                    ? [
                        {
                          name: 'recipient',
                          title: 'Ontvanger',
                          label: getSubjectLabel(recipient),
                          url: getSubjectUrl(recipient),
                          openExternal: true,
                          icon: 'account-check',
                          type: 'link',
                        },
                      ]
                    : []),
                  {
                    name: 'assignee',
                    title: 'Behandelaar',
                    type: !assignee ? 'button' : 'link',
                    label: getAssigneeLabel(assignee, canAssign),
                    url: assignee ? getSubjectUrl(assignee) : null,
                    openExternal: true,
                    icon: 'account-star-variant',
                    click: ctrl.onSelfAssign,
                    disabled: !!assignee || !canAssign,
                  },
                  {
                    name: 'department',
                    title: 'Afdeling',
                    label: get(casetype, 'instance.department'),
                    url: null,
                    icon: 'source-fork',
                  },
                  {
                    name: 'registration_date',
                    title: 'Registratiedatum',
                    label: transformDate(registrationDate),
                    url: null,
                    icon: 'calendar-plus',
                  },

                  {
                    name: 'target_date',
                    title: 'Streefafhandeldatum',
                    label: transformDate(targetDate),
                    url: null,
                    icon: 'calendar-check',
                    style: { 'passed-due-date': isPassedDueDate(targetDate) },
                  },
                  ...(caseClosed
                    ? [
                        {
                          name: 'date_of_completion',
                          title: 'Afhandeldatum',
                          label: transformDate(completionDate),
                          icon: 'calendar-check',
                        },
                      ]
                    : []),
                  ...(location
                    ? [
                        {
                          name: 'case_location',
                          url: $state.href('case.location', null, {
                            inherit: true,
                          }),
                          openExternal: false,
                          title: 'Zaakadres',
                          icon: 'map-marker',
                          label: getLocationLabel(location),
                        },
                      ]
                    : []),
                  {
                    name: 'confidentiality',
                    type: 'button',
                    title: 'Vertrouwelijkheid',
                    label: confidentiality.mapped,
                    url: null,
                    icon:
                      confidentiality.original === 'confidential'
                        ? 'eye-off'
                        : 'eye',
                    click: openConfidentialityModal,
                    disabled: !canChangeConfidentiality,
                    style: {
                      confidential: confidentiality.original === 'confidential',
                    },
                  },
                  ...(!!caseObj.instance.price && !!paymentStatus
                    ? [
                        {
                          name: 'payment_amount',
                          title: getPaymentStatusTitle(paymentStatus),
                          label: caseObj.instance.price,
                          icon: 'currency-eur',
                          secondaryIcon: getPaymentStatusIcon(
                            paymentStatus.original
                          ),

                          style: `payment-status-${paymentStatus.original}`,
                        },
                      ]
                    : []),
                  {
                    name: 'about',
                    title: 'Meer informatie',
                    label: 'Meer informatie',
                    url: $state.href(
                      auxiliaryRouteService.append(currentState, 'case.info'),
                      null,
                      { inherit: true }
                    ),
                    openExternal: false,
                    icon: 'dots-horizontal',
                  },
                ];

                return links.map(mapLink(isCollapsed));
              }
            );

            confidentialityReducer = composedReducer(
              { scope: $scope },
              ctrl.case
            ).reduce((caseObj) => get(caseObj, 'instance.confidentiality'));

            confOptionsReducer = composedReducer(
              { scope: $scope },
              confidentialityOptions,
              confidentialityReducer
            ).reduce((options, confidentiality) => {
              return options.map((option) => {
                return assign({}, option, {
                  classes: {
                    selected: option.name === confidentiality.original,
                  },
                  click: () => {
                    ctrl.onConfidentialityChange({
                      $confidentiality: option.name,
                    });
                  },
                });
              });
            });

            ctrl.getLinks = linkReducer.data;

            ctrl.getConfidentialityOptions = confOptionsReducer.data;

            ctrl.getConfidentiality = () =>
              get(confidentialityReducer.data(), 'original');
            ctrl.getConfidentialityLabel = () =>
              get(confidentialityReducer.data(), 'mapped');

            ctrl.openInNewWindow = (link, event) => {
              // Dirty way to open a window but $window.open(url, target, 'noopener')
              // opens a new window instead of a new tab on Chrome
              let linkElement = document.createElement('a');
              linkElement.target = '_blank';
              linkElement.href = link;
              linkElement.rel = 'noopener';
              document.body.appendChild(linkElement);
              linkElement.click();
              linkElement.parentNode.removeChild(linkElement);

              event.preventDefault();
              event.stopPropagation();
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
