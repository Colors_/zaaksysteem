// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default {
  defaults: {
    copy_relations: true,
    has_pattern: false,
    pattern: {
      every: {
        count: 1,
        type: 'days',
      },
      repeat: 1,
    },
  },
  fields: [
    {
      name: 'casetype',
      template: 'object-suggest',
      label: 'Zaaktype',
      data: {
        objectType: 'casetype',
      },
      required: true,
    },
    {
      name: 'copy_relations',
      template: 'checkbox',
      label: 'Objectrelaties kopiëren',
      data: {
        checkboxLabel: 'Ja',
      },
    },
    {
      name: 'from',
      template: 'date',
      label: 'Vanaf',
      required: true,
    },
    {
      name: 'has_pattern',
      template: 'checkbox',
      label: 'Terugkeerpatroon',
      data: {
        checkboxLabel: 'Ja',
      },
    },
    {
      name: 'pattern',
      template: 'form',
      label: 'Herhaling',
      when: ['$values', (values) => !!values.has_pattern],
      data: {
        fields: [
          {
            name: 'every',
            label: 'Elke',
            template: 'form',
            required: true,
            data: {
              fields: [
                {
                  name: 'count',
                  template: 'number',
                  required: true,
                },
                {
                  name: 'type',
                  template: 'select',
                  required: true,
                  data: {
                    options: [
                      {
                        value: 'days',
                        label: 'Dagen',
                      },
                      {
                        value: 'weeks',
                        label: 'Weken',
                      },
                      {
                        value: 'months',
                        label: 'Maanden',
                      },
                      {
                        value: 'years',
                        label: 'Jaren',
                      },
                    ],
                  },
                },
              ],
            },
          },
          {
            name: 'repeat',
            label: 'Aantal herhalingen',
            template: 'number',
            required: true,
          },
        ],
      },
    },
  ],
};
