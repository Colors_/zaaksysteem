package Zaaksysteem::Test::Backend::Sysin::Modules::Roles::ClientCertificate;

use Zaaksysteem::Test;
use Zaaksysteem::Test::Mocks;

sub test_client_certificate_check {

    my $test_module = ZS::Test::ClientCert->new();

    my $cert = mock_strict(fingerprint_sha256 => 1);

    my ($file_object, $string_object);
    my $override = override('Crypt::OpenSSL::X509::new_from_file' => sub { shift; $file_object = shift ; return $cert});
    $override->override('Crypt::OpenSSL::X509::new_from_string' => sub { shift; $string_object = shift ; return $cert});

    throws_ok(
        sub {
            $test_module->assert_client_certificate();
        },
        qr/missing: certificates/,
        "Empty profile test"
    );

    my $ok = $test_module->assert_client_certificate(
        client_cert => 'my cert',
        stored_cert => 'stored cert',
    );
    ok($ok, "assert_client_certificate works with require_some");

    $ok = $test_module->assert_client_certificate(
        client_cert => 'my cert',
        get_certificate_cb => sub { mock_strict(get_path => { fingerprint_sha256  => 1 }) },
    );
    ok($ok, "assert_client_certificate works with require_some");

}

package ZS::Test::ClientCert;
use Moose;
with qw(MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
