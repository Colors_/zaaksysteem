package Zaaksysteem::Test::Scheduler;

use Moose;

extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Scheduler;
use Zaaksysteem::Test;

sub test_scheduler_install_jobs {
    my $job_count = 0;
    my @collected_jobs;
    my @creates;
    my @loglines;

    my $scheduler = mock_one(
        c => mock_one(
            collect_script_retvals => sub {
                return @collected_jobs;
            },
            model => mock_one(
                count => sub { return $job_count },
                save_object => sub { push @creates, { @_ } }
            ),
        ),
        info => sub { push @loglines, shift }
    );

    lives_ok {
        Zaaksysteem::Scheduler::install_default_jobs($scheduler)
    } 'installer with 0 jobs';

    push @collected_jobs, mock_one(
        'X-Mock-ISA' => 'Zaaksysteem::Object::Types::ScheduledJob'
    );

    lives_ok {
        Zaaksysteem::Scheduler::install_default_jobs($scheduler)
    } 'installer with 1 job';

    is scalar @creates, 1, 'one job installed';
    is scalar @loglines, 1, 'one logline pushed';
    like $loglines[0], qr/Installing scheduled job/, 'info line is useful';

    my $first = $creates[0]{ object };
    ok defined $first && $first->isa('Zaaksysteem::Object::Types::ScheduledJob');

    push @collected_jobs, 1;

    my $ex = exception {
        Zaaksysteem::Scheduler::install_default_jobs($scheduler)
    };

    is $ex->type, 'scheduled/install_default_jobs/job_not_valid',
        'installer catches badly behaved scripts';

    pop @collected_jobs;

    is scalar @creates, 2, 'job inserted when count is 0';

    pop @creates;

    $job_count = 1;

    lives_ok {
        Zaaksysteem::Scheduler::install_default_jobs($scheduler)
    } 'installer with 1 previously installed job';

    is scalar @creates, 1, 'job does not double-insert';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
