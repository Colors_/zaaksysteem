package Zaaksysteem::Model::AMQP;
use Moose;
use namespace::autoclean;

use Net::AMQP::RabbitMQ;
use Zaaksysteem::StatsD;

with 'Catalyst::Component::InstancePerContext';

=head1 NAME

Zaaksysteem::Model::AMQP - Model for AMQP connections

=head1 SYNOPSIS

    package My::Something;
    use Moose;

    sub x {
        my $mq = $c->model('AMQP'); # returns a Net::AMQP::RabbitMQ object
        $mq->publish(...);
    }

=cut

=head1 METHODS

=head2 build_per_context_instance

Create a new connection to the configured AMQP server for this context/request.

Returns a L<Net::AMQP::RabbitMQ> instance.

=cut

sub build_per_context_instance {
    my $self = shift;
    my $c = shift;

    $c->log->debug("Building new RabbitMQ instance");

    my $mq_connection = Net::AMQP::RabbitMQ->new();

    my $channel = $c->config->{'Model::Queue'}{channel} // 1;

    my $rabbitmq_host = $c->config->{'Model::Queue'}{host};
    my $rabbitmq_opts = $c->config->{'Model::Queue'}{options} // {};

    if ($ENV{ ZS_SILO_NAME }) {
        $rabbitmq_opts->{ vhost } = $ENV{ ZS_SILO_NAME };
    }

    my $t0 = $c->statsd->start;
    $mq_connection->connect($rabbitmq_host, $rabbitmq_opts);
    $mq_connection->channel_open($channel);
    $c->statsd->end('amqp_connect_duration', $t0);
    $c->statsd->increment('amqp_connect_number', $t0);

    return $mq_connection;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, 2021 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
