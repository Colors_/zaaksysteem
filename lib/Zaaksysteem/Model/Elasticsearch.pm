package Zaaksysteem::Model::Elasticsearch;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory';

__PACKAGE__->config(
    class => 'Search::Elasticsearch',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::Elasticsearch - Catalyst model factory for
L<Search::Elasticsearch>.

=head1 DESCRIPTION

This model factory produces instances of L<Search::Elasticsearch> using the
app's C<zaaksysteem.conf> configuration.

    my $es = $c->model('Elasticsearch', { cluster => 'ClusterRef' });

Zaaksysteem supports using multiple Elasticsearch clusters, which can be
configured in the main configuration file. For this reason, the cluster option
is required for every call.

=head2 Model configuration example

    # zaaksysteem.conf
    <Search::Elasticsearch>
        <ClusterRef>
            nodes = [ cluster-host1:1337 ]
        </ClusterRef>
        <AnotherCluster>
            nodes = ...
        </AnotherCluster>
    </Search::ElasticSearch>

=cut

use BTTW::Tools;

=head1 METHODS

=head2 prepare_arguments

Implements logic for L<Catalyst::Model::Factory::PerRequest>.

Aggregates configuration for the constructor of L<Search::Elasticsearch>.

=cut

define_profile prepare_arguments => (
    required => {
        cluster => 'Str'
    }
);

sub prepare_arguments {
    my $self = shift;
    my $c = shift;
    my $opts = assert_profile(shift)->valid;

    my $config = $c->config->{ 'Search::Elasticsearch' } || {};

    # Empty config is a valid param set for ES client instantiation
    return $config->{ $opts->{ cluster } } || {};
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
