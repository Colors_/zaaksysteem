package Zaaksysteem::Model::Transaction;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

=head1 NAME

Zaaksysteem::Model::Transaction - Per Request wrapper around Zaaksysteem::Transaction

=head1 SYNOPSIS

    my $model = $c->model('Transaction');

=head1 CONFIGURATION

This model configures the transaction model using the "Transaction" block in the
configuration:

    <Transaction>
        key = value
    </Transaction>

=cut

__PACKAGE__->config(
    class       => 'Zaaksysteem::Transaction',
    constructor => 'new',
);

=head1 METHODS

=head2 prepare_arguments

Returns the arguments to create a new L<Zaaksysteem::Transactions> instance.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        schema => $c->model('DB')->schema,
        $c->config->{Transaction} ? %{$c->config->{Transaction}} : (),
    };
}

__PACKAGE__->meta->make_immutable;

=head1 SEE ALSO

L<Zaaksysteem::Transaction>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018-2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
