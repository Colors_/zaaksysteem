package Zaaksysteem::Object::Syntax;

use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Object::Syntax - Object syntax helpers

=head1 SYNOPSIS

    package My::Fancy::Package;

    use Zaaksysteem::Object::Syntax;

    sub some_method {
        ...
        my $object_value = szg_val('string', 'my_value');
    }

=cut

use BTTW::Tools;
use Moose::Exporter;

use Zaaksysteem::Object::Query;
use Zaaksysteem::Object::Reference::Instance;
use Zaaksysteem::Object::ValueModel;

Moose::Exporter->setup_import_methods(
    also => 'Zaaksysteem::Object::Query',
    as_is => [qw[
        szg_value
        szg_object_ref
    ]]
);

=head1 FUNCTIONS

=head2 szg_value

Construct a new L<Zaaksysteem::Object::Value> instance.

    my $val = szg_value('datetime', Datetime->now->substract(days => 5));

=cut

sub szg_value {
    return Zaaksysteem::Object::ValueModel->new_value(@_);
}

=head2 szg_object_ref

Construct a new L<Zaaksysteem::Object::Value> using the
L<Zaaksysteem::Object::ValueType::ObjectRef> type.

    # $val->value will be a Zaaksysteem::Object::Reference::Instance
    my $val = szg_object_ref('case', '1728ab08-ba50-40b1-a4cc-4ff8ab78234e');

=cut

sub szg_object_ref {
    return Zaaksysteem::Object::ValueModel->new_value(
        object_ref => Zaaksysteem::Object::Reference::Instance->new(
            type => shift,
            id => shift
        )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
