package Zaaksysteem::Object::Queue::Model::AdvancedSearch;

use Moose::Role;

requires qw(log);

use BTTW::Tools;
use Zaaksysteem::Search::Object;
use Zaaksysteem::Search::Case::Object;
use Zaaksysteem::Export::Model;
use Zaaksysteem::Export::CSV;
use Zaaksysteem::Export::TopX;

=head1 NAME

Zaaksysteem::Object::Queue::Model::AdvancedSearch - AdvancedSearch queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 export_advanced_search

=cut

sig export_advanced_search => 'Zaaksysteem::Backend::Object::Queue::Component';

sub export_advanced_search {
    my $self = shift;
    my $item = shift;
    $self->_log_export("Uitgebreid zoeken", '_export_advanced_search', $item);
}

sub _log_export {
    my $self   = shift;

    my $type   = shift;
    my $method = shift;
    my $item   = shift;

    my ($filename, $error);
    try {
        $filename = $self->$method($item);
    }
    catch {
        $error = $_;
    };

    my $user = $item->subject;
    $self->logging->trigger(
        'export/advanced_search',
        {
            component => "export",
            data => {
                export  => $type,
                user_id => $user->id,
                username => $user->username,
                $filename ? (
                    filename => $filename,
                ) : (),
                $error ? (
                    error => "$error"
                ) : (),
            }
        }
    );

    # Finally die a horrible death
    die $error if $error;
    return;
}

sub _export_advanced_search {
    my $self = shift;
    my $item = shift;

    my $data = $item->data;

    my $user = $item->subject;

    my $format = $data->{format};

    my $model = $self->_get_search_model($user, $data->{zql});

    my $results = $model->search();

    # TODO: Rename to Export::Format
    my $formatting = Zaaksysteem::Export::CSV->new(
        user              => $user,
        blacklist_mapping => $model->blacklisting,
        objects           => $results,
        format            => $format,
    );

    $formatting->process;

    my $fh = $formatting->to_filehandle();

    my $export = Zaaksysteem::Export::Model->new(
        schema     => $self->schema,
        user       => $user,
        request_id => $item->metadata->{request_id},
        uri        => URI->new_abs('/exportqueue', $self->base_uri),
        type       => 'search',
    );

    # calc is ods.. so yeah
    my $filename = $export->generate_filename(
        'uitgebreid-zoeken', $format eq 'calc' ? 'ods' : $format,
    );

    $export->add_as_export($fh, $filename);
    return $filename;
}

sub _get_search_model {
    my ($self, $user, $zql) = @_;

    my $model = try {
        my $model = Zaaksysteem::Search::Case::Object->new(
            user   => $user,
            schema => $self->schema,
            query  => $zql,
        );
        # This check is fatal on this type of model, since the BUILD of the
        # ZQL parser in this bit croaks when it is not a case.
        $model->check_zql_command_for('case');
        return $model;
    }
    catch {
        # allow non-case search to pass through
        return if $_ =~ m#^case/zql/object_type:#;
        $self->log->info("$_");
        die $_;
    };
    return $model if $model;

    my $om = Zaaksysteem::Object::Model->new(
        table  => 'ObjectData',
        schema => $self->schema,
        user   => $user,
    );

    return Zaaksysteem::Search::Object->new(
        user         => $user,
        schema       => $self->schema,
        query        => $zql,
        object_model => $om,
    );
}


sig export_document_list => 'Zaaksysteem::Backend::Object::Queue::Component';

sub export_document_list {
    my $self = shift;
    my $item = shift;
    $self->_log_export("Documentlijst", '_export_document_list', $item);
}

sub _export_document_list {
    my $self = shift;
    my $item = shift;

    my $data = $item->data;

    my $user = $item->subject;

    my $model = $self->_get_search_model($user, $data->{zql});

    my $format = 'csv';

    my $results = $model->search_documents();
    my @objects;
    while (my $zaak = $results->next) {
        push(@objects, $zaak->export_files);
    }

    my $formatting = Zaaksysteem::Export::CSV->new(
        user              => $user,
        blacklist_mapping => {},
        objects           => \@objects,
        format            => $format,
        column_order      => [
            qw(
                Bestandsnaam
                Bestandstype
                Aangemaakt
                Reden
                Status
                Documentstatus
                ZaakID
                Versie
            )
        ],
    );

    $formatting->process;

    my $fh = $formatting->to_filehandle();

    my $export = Zaaksysteem::Export::Model->new(
        schema     => $self->schema,
        user       => $user,
        request_id => $item->metadata->{request_id},
        uri        => URI->new_abs('/exportqueue', $self->base_uri),
        type       => 'search',
    );

    my $filename = $export->generate_filename(
        'documentlijst', $format,
    );

    $export->add_as_export($fh, $filename);
    return $filename;
}

sig export_tmlo_topx => 'Zaaksysteem::Backend::Object::Queue::Component';

sub export_tmlo_topx {
    my $self = shift;
    my $item = shift;
    $self->_log_export("TopX", '_export_tmlo_topx', $item);
}

sub _export_tmlo_topx {
    my ($self, $item) = @_;
    my $data = $item->data;

    my $user = $item->subject;

    my $model = $self->_get_search_model($user, $data->{zql});

    my $interface = $self->_find_topx_interface();
    my $mapping   = $self->_get_topx_mapping($interface);

    my $config        = $interface->get_interface_config;
    my $type          = $config->{export_type} // 'sidecar';
    my $export_id     = $config->{export_id};
    my $export_target = $config->{export_target};

    my $results      = $model->search();

    my $fh = File::Temp->new();
    my $tar = Archive::Tar::Stream->new(outfh => $fh);

    my $topx = Zaaksysteem::Export::TopX->export_type(
        export_type => $type,
        schema      => $self->schema,
        user        => $user,
        objects     => $results,
        tar         => $tar,
        $export_id     ? (identification    => $export_id)     : (),
        $export_target ? (target_system     => $export_target) : (),
        $mapping       ? (attribute_mapping => $mapping)       : (),
    );

    $topx->export;

    my $export = Zaaksysteem::Export::Model->new(
        schema     => $self->schema,
        user       => $user,
        request_id => $item->metadata->{request_id},
        uri        => URI->new_abs('/exportqueue', $self->base_uri),
        type       => 'tmlo',
    );

    my $filename = $export->generate_filename('tmlo-topx', 'tar');

    $export->add_as_export($fh, $filename);
    return $filename;

}

sub _find_topx_interface {
    my ($self, $interface) = @_;
    my $rs = $self->build_resultset('Interface');
    return $rs->search_active({module => 'export_mapping_topx'})->single;
}

sub _get_topx_mapping {
    my ($self, $interface) = @_;
    return unless $interface;
    my $mapping = $interface->get_interface_config->{attribute_mapping};
    my @active_mapping;
    foreach (@$mapping) {
        next unless $_->{checked};
        next unless defined $_->{internal_name};
        next unless defined $_->{internal_name}{searchable_object_id};
        push(
            @active_mapping,
            {
                external_name => $_->{external_name},
                internal_name => $_->{internal_name}{searchable_object_id}
            }
        );
    }
    return \@active_mapping if @active_mapping;
    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
