package Zaaksysteem::Object::Import;
use Moose;

use Zaaksysteem::Object::ImportState;
use Zaaksysteem::Object::Importer;
use Zaaksysteem::Object::Model;

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

has object_model => (
    is      => 'ro',
    isa     => 'Zaaksysteem::Object::Model',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return Zaaksysteem::Object::Model->new(
            schema     => $self->schema,
        );
    },
);

has filestore => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Model::DB::Filestore',
    required => 1,
);

has importer => (
    is     => 'ro',
    isa    => 'Zaaksysteem::Object::Importer',
    writer => '_importer',
);

has state => (
    is     => 'ro',
    isa    => 'Zaaksysteem::Object::ImportState',
    writer => '_state',
);

has file_format => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has object_type => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 run

Arguments: none

Return value: none

    $obj->run()

Will import objects from the set $self->filestore.

=cut

sub run {
    my $self = shift;

    my $fileref = $self->filestore;

    my $state = Zaaksysteem::Object::ImportState->new(library_id => 1,);

    my $importer = Zaaksysteem::Object::Importer->new(
        format      => $self->file_format,
        object_type => $self->object_type,
        schema      => $self->schema,
        state       => $state,
        model       => $self->object_model,
    );

    $importer->hydrate_from_files($fileref);
    $self->_importer($importer);
    $self->_state($state);
    $importer->execute;
    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Object::Import - A Zaaksysteem::Object::Import package

=head1 DESCRIPTION

=head1 SYNOPSIS

    my $importer = Zaaksysteem::Object::Import->new(
        schema      => $schema,
        filestore   => $file->filestore_id,
        file_format => 'Sdu::Faq',
        object_type => $object_type,
    );
    $importer->run();


=head1 METHODS

=head2 run

Run the importer

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
