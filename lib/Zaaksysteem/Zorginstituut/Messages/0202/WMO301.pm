package Zaaksysteem::Zorginstituut::Messages::0202::WMO301;
use Moose;

extends 'Zaaksysteem::Zorginstituut::Message';
with qw(
    Zaaksysteem::Zorginstituut::Roles::301
);

=head1 NAME

Zaaksysteem::Zorginstituut::Messages::0202::WMO301 - WMO 2.2 301 message

=head1 DESCRIPTION

A sending module for WMO301 messages

=head1 SYNOPSIS

    use Zaaksysteem::Zorginstituut::Messages::0202::WMO301;

    my $wmo301 = Zaaksysteem::Zorginstituut::Messages::0202::WMO301->new(
        case => $case
    );

    my $xml = $wmo301->encode();

=cut

use BTTW::Tools;
use Zaaksysteem::XML::Compile;

has '+message_version' => ( default => 2 );
has '+message_sub_version' => ( default => 2);
has '+code' => ( default => 414 );


=head1 PRIVATE METHODS and BUILDERS

=head2 _build_encoder

Builds the encoder for this type of message, used by the C<encoder> attribute

=cut

sub _build_encoder {
    my $self = shift;

    return Zaaksysteem::XML::Compile->xml_compile->add_class(
        'Zaaksysteem::XML::Generator::WMO::0202'
    )->iwmo_0202;

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
