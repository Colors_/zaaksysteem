package Zaaksysteem::Search::Conditional::IsNull;

use Moose::Role;

=head1 NAME

Zaaksysteem::Search::Conditional::IsNull - Special case for IS NULL conditions

=head1 METHODS

=head2 evaluate

Overriden evaluate behavior (see L<Zaaksysteem::Search::Conditional>).

Since defined-ness checks (IS NOT NULL) for HStore properties require special
syntax, this role provides an evaluate method that returns some special case
SQL string.

    $is_not_null_cond->evaluate()
    => (defined(hstore_column, key) IS TRUE)

Undefinedness checks (IS NULL) are supported by using "key => NULL", so those
are forwarded to the regular L<Zaaksysteem::Search::Conditional> code.

=cut

around evaluate => sub {
    my $orig = shift;
    my $self = shift;
    my $rs = shift;

    return \[
        sprintf(
            "(defined(%s, ?) IS %s)",
            $rs->hstore_column,
            $self->invert ? 'TRUE' : 'FALSE'
        ),
        [ {} => $self->lterm->value ]
    ];
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

