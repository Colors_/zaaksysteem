package Zaaksysteem::Search::Case::ES;
use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::Search::Case::ES - ES for Case

=head1 DESCRIPTION

A migration class for Zaaksysteem::Search::ESQuery to remove the dependency to object data

=head1 SYNOPSIS

    use Foo;

=cut

extends 'Zaaksysteem::Search::ESQuery';

sub _prepare_resultset {
    my ($self, $rs) = @_;
    return $rs;
}

sub _apply_to_resultset {
    my ($self, $rs) = @_;
    my $cond = $self->parse($self->query);
    return $rs->search_rs($cond->evaluate($rs));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
