package Zaaksysteem::Search::ZQL::Literal::Function;

use Moose;

use DateTime;
use Time::Duration::Parse;

use Zaaksysteem::Search::Term::Function;
use Zaaksysteem::Search::Term::Literal;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'Str' );
has arguments => ( is => 'ro', isa => 'Zaaksysteem::Search::ZQL::Literal::Set', required => 1 );

my %LOCAL_FUNCTIONS = (
    now => sub {
        return DateTime->now;
    },
    past => sub {
        my $args = shift;

        my $seconds = _parse_duration($args->value->[0]->value);

        return DateTime->now->subtract(seconds => $seconds);
    },
    future => sub {
        my $args = shift;

        my $seconds = _parse_duration($args->value->[0]->value);

        return DateTime->now->add(seconds => $seconds);
    },
);

override new_from_production => sub {
    my $class = shift;

    return $class->new(value => shift, arguments => shift);
};

override dbixify => sub {
    my $self = shift;
    my $cmd = shift;

    my $function = lc($self->value);

    if (exists $LOCAL_FUNCTIONS{ $function }) {
        return Zaaksysteem::Search::Term::Literal->new(
            value => $LOCAL_FUNCTIONS{ $function }->($self->arguments)
        );
    }

    return Zaaksysteem::Search::Term::Function->new(
        object_type => $cmd->object_type,
        function => $function,
        arguments => $self->arguments->dbixify
    );
};

sub _parse_duration {
    my $string = shift;

    return try {
        return parse_duration($string);
    } catch {
        throw('search/zql/invalid_duration', sprintf(
            '"%s" is not a valid duration string',
            $string
        ));
    };
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

