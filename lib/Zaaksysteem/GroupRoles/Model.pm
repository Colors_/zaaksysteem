package Zaaksysteem::GroupRoles::Model;
use Moose;

use List::Util qw(first any);

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

has groups => (
    is       => 'ro',
    isa      => 'ArrayRef',
    lazy     => 1,
    builder  => '_build_groups',
    init_arg => undef,
);

has roles => (
    is       => 'ro',
    isa      => 'ArrayRef',
    lazy     => 1,
    builder  => '_build_roles',
    init_arg => undef,
);

sub _build_roles {
    my $self = shift;
    return $self->schema->resultset('Roles')->get_all_cached();
}

sub _build_groups {
    my $self = shift;
    return $self->schema->resultset('Groups')->get_all_cached();
}

sub get_role_by_id {
    my ($self, $id) = @_;
    return first { $id == $_->id } @{ $self->roles };
}

sub get_role_by_name {
    my ($self, $name, $group) = @_;

    if ($group) {
        foreach my $role (@{$self->roles}) {
            next if $role->name ne $name;
            my $pid = $role->get_column('parent_group_id');
            return $role if any { $pid == $_ } @{ $group->path };
        }
    }

    return first { $name eq $_->name } @{ $self->roles };
}

sub get_group_by_id {
    my ($self, $id) = @_;
    return first { $id == $_->id } @{ $self->groups };
}

sub get_group_by_name {
    my ($self, $name) = @_;
    return first { $name eq $_->name } @{ $self->groups };
}



__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::GroupRoles::Model;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
