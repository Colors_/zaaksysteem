package Zaaksysteem::Backend::ZaakMeta::ResultSet;

use Zaaksysteem::Moose;
use feature 'state';

extends 'Zaaksysteem::Backend::ResultSet';
with 'Zaaksysteem::Roles::ZQL';

=head1 NAME

Zaaksysteem::Backend::ZaakMeta::ResultSet - Convenient functions for zaak_meta

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaak Meta within zaaksysteem functions

=head1 REQUIRED ATTRIBUTES

=head1 METHODS

=cut

sub get_stable_sort_key { return }

sub map_native_column {
    my ($self, $k) = @_;
    state %mapping = (
        'case.number' => 'me.zaak_id',
    );

    if (exists $mapping{$k}) {
        return $mapping{$k};
    }
    return;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
