package Zaaksysteem::Backend::Sysin::Modules::LegacyPublicatie;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    /;


=head1 INTERFACE CONSTANTS

=head2 INTERFACE_ID

=head2 INTERFACE_CONFIG_FIELDS

=head2 MODULE_SETTINGS

=cut

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use Zaaksysteem::ZAPI::Form::Field;

use constant INTERFACE_ID            => 'legacy_publicaties';
use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_internal_name',
        type        => 'text',
        label       => 'Interne publicatienaam',
        required    => 1,
        description => 'De interne naam voor de publicatie waar specifiek gedrag van de koppeling wordt getriggerd',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_protocol',
        type        => 'select',
        label       => 'Protocol',
        required    => 1,
        description => 'Protocol dat wordt gebruikt voor het overdragen van bestanden',
        data => {
            options => [
                {
                    label => 'FTP',
                    value => 'ftp',
                },
                {
                    label => 'Secure File Copy',
                    value => 'scp',
                },
            ],
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_username',
        type        => 'text',
        label       => 'Username',
        required    => 1,
        description => 'Username voor de remote server',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_password',
        type        => 'password',
        label       => 'Wachtwoord',
        required    => 1,
        description => 'Wachtwoord voor de remote server',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_hostname',
        type        => 'text',
        label       => 'Hostname',
        required    => 1,
        description => 'Hostname van de remote server',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_port',
        type        => 'text',
        label       => 'Poort',
        required    => 1,
        description => 'Poort van de remote server',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_source_directory',
        type        => 'text',
        label       => 'Bronmap',
        required    => 1,
        description => 'Brondirectory',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_destination_directory',
        type        => 'text',
        label       => 'Doelmap',
        required    => 0,
        description => 'Doelmap',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_csv_filename',
        type        => 'text',
        label       => 'CSV bestandsnaam',
        description => 'De bestandsnaam van de CSV',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_notify_url',
        type        => 'text',
        label       => 'URL',
        required    => 1,
        description => 'URL waar een bericht naartoe wordt verzonden zodra de upload klaar is',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_notify_wait',
        type        => 'text',
        label       => 'Notificatie wachtperiode',
        default     => 2,
        description => 'Het aantal seconden voordat we na de publicatie wachten met het opsturen van de notificatie',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_notify_filename',
        type        => 'text',
        label       => 'Notificatie bestandsnaam',
        description => 'De bestandsnaam voor de file die aangeeft dat er nieuwe publicaties zijn',
    ),
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Publicaties via SCP',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    manual_type                   => ['text'],
    is_multiple                   => 1,
    is_manual                     => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    attribute_list                => [],
    retry_on_error                => 1,
    trigger_definition            => { },
};

has exception => (is => 'rw');

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{ MODULE_SETTINGS() });
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
