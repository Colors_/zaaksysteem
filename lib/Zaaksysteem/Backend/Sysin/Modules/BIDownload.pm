package Zaaksysteem::Backend::Sysin::Modules::BIDownload;
use Zaaksysteem::Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';
with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::BaseURI
    Zaaksysteem::Backend::Sysin::Modules::Roles::CertificateInfo
    Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
);

use Crypt::OpenSSL::Random qw(random_pseudo_bytes);
use URI::Escape qw(uri_unescape);
use Zaaksysteem::External::S3;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::BIDownload - Integration config for BI downloads

=cut

=head1 METHODS

=head2 BUILDARGS

Wrapper to inject the module settings into newly-created instances.

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(
        name => 'bidownload',
        label => 'BI Downloads',
        description => <<EOD,
    <p>
        Hier kunt u de BI download API-koppeling configureren.
    </p>
EOD
        interface_config => [
            Zaaksysteem::ZAPI::Form::Field->new(
                name     => 'interface_client_certs_enabled',
                type     => 'checkbox',
                default  => 1,
                label    => 'Gebruik client-certificaat',
                description => <<'EOD'
        <p>
            Als deze optie uit staat, verreist de koppeling geen client-certificaat
            voor het ophalen van BI-data.
        </p>
        <p>
            Let op: dit is minder veilig! Genereer regelmatig een nieuwe API-sleutel.
        </p>
EOD
            ),
            Zaaksysteem::ZAPI::Form::Field->new(
                name        => 'interface_security_warning',
                type        => 'display',
                label       => 'Waarschuwing',
                when        => '!interface_client_certs_enabled',
                data        => {
                    template => 'Let op! Zonder client-certificaat is deze integratie minder veilig. Genereer regelmatig een nieuwe API-key.'
                },
            ),

            Zaaksysteem::ZAPI::Form::Field->new(
                name     => 'interface_client_certificate',
                type     => 'file',
                required => 1,
                label    => 'Client-certificaat',
                when => 'interface_client_certs_enabled',
                description => <<'EOD'
        <p>
            Upload hier het certificaat (publiek deel) wat de download-applicatie zal
            gebruiken om zich te authenticeren bij aanroep van de BI API.
        </p>
EOD
            ),
            Zaaksysteem::ZAPI::Form::Field->new(
                name     => 'interface_api_key',
                type     => 'text',
                label    => 'API key',
                data => {
                    template => '<[field.value]>'
                },
                description => <<'EOD'
        <p>
            API key die de download-applicatie icm. het client certificate moet gebruiken
            om zich te authenticaren bij aanroep van de BI API.
        </p>
EOD
            ),
            Zaaksysteem::ZAPI::Form::Field->new(
                name        => 'interface_bi_api_get_url',
                type        => 'display',
                label       => 'API URL voor download',
                data => {
                    template => '<[field.value]>'
                },
            ),
        ],

        direction => 'incoming',
        manual_type => [],
        is_multiple => 0,
        is_manual => 0,
        has_attributes => 0,
        retry_on_error => 0,
        is_casetype_interface => 0,
        allow_multiple_configurations => 0,

        trigger_definition => {
            download => {
                method => 'download_dump',
                api => 1,
            },
        },

        test_interface => 0,
        test_definition => {},

        interface_update_callback => sub {
            return shift->_update_interface_config(@_);
        },
    );
};

has uri_values => (
    is => 'ro',
    isa => 'HashRef',
    default => sub { {} }
);

has services_uri_values => (
    is => 'ro',
    isa => 'HashRef',
    default => sub {
        {
            interface_bi_api_get_url => "/api/v1/sysin/interface/UUID/trigger/download"
        }
    }
);

sub _update_interface_config {
    my ($module, $interface, $model) = @_;

    my $config = $interface->get_interface_config;

    $module->get_certificate_info(
        $interface,
        $config,
        qw(client_certificate)
    );

    $config->{api_key} ||= unpack('H*', random_pseudo_bytes(32));

    $interface->update_interface_config($config);
}

sub download_dump {
    my ($self, $params, $interface) = @_;

    my $schema = $interface->result_source->schema;

    my $config = $schema->catalyst_config->{"Plugin::Zaaksysteem::Bidumper"};
    my $bucket_name = $config->{"bucket"};
    my $s3_config = $config->{"constructor_arguments"};

    my $today = DateTime->now()->strftime('%Y-%m-%d');
    my $logging_id = $schema->customer_config->{logging_id};

    $self->_assert_api_key($interface->jpath('$.api_key'), $params->{"headers"});

    my $transaction = $interface->process(
        {
            processor_params => {
                processor => '_download_dump',
                request_params => {
                    today => $today,
                    logging_id => $logging_id,
                },
            },
            input_data => "Download request"
        }
    );
    throw(
        'sysin/modules/bidownload/download_dump',
        'Failure in getting results',
        { http_code => 500 },
    ) unless $transaction->processor_params && $transaction->processor_params->{result};

    my $filename = $transaction->processor_params->{result};
    my $s3 = Zaaksysteem::External::S3->build_s3(%$s3_config);
    my $bucket = $s3->bucket($bucket_name);

    my $exists = $bucket->head_key($filename);
    unless ($exists) {
        throw(
            'sysin/modules/bidownload/file_not_found',
            'Dump file not found.',
            { http_code => 404 },
        )
    }

    my $download_url = $bucket->query_string_authentication_uri($filename, time + 60);
    $self->log->trace("Presigned URL for S3 download: $download_url");

    return {
        type => "download",
        download_url => Zaaksysteem::External::S3->transform_download_url($download_url),
    };
}

sub _assert_api_key {
    my ($self, $configured_key, $headers) = @_;

    if ($headers->{"api-key"} ne $configured_key) {
        throw(
            "sysin/modules/bidownload/api_key_mismatch",
            "Supplied API key does not match configured value.",
            { http_code => 403 },
        );
    }

    return;
}

sub _download_dump {
    my $self = shift;
    my $record = shift;

    my $transaction = $self->process_stash->{transaction};
    my $params = $transaction->get_processor_params()->{request_params};

    my $today = $params->{today};
    my $instance_name = $params->{logging_id};

    my $filename = "$instance_name-$today-cases.json.gz";

    $transaction->processor_params(
        {
            %{ $transaction->processor_params },
            result => $filename,
        }
    );

    $record->input("API request to download cases dump");
    $record->output($filename);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
