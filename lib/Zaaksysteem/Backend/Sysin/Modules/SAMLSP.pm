package Zaaksysteem::Backend::Sysin::Modules::SAMLSP;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::MultiTenant
    Zaaksysteem::Backend::Sysin::Modules::Tests::SAML
    Zaaksysteem::Backend::Sysin::Modules::Roles::CertificateInfo
);

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID => 'samlsp';

sub build_config_fields {
    my $self   = shift;
    my @fields = (
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_sp_cert',
            type     => 'file',
            label    => 'Public key van het PKIoverheid certificaat',
            required => 1,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_sp_key',
            type     => 'file',
            label    => 'Private key van het PKIoverheid certificaat',
            required => 1,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_contact_id',
            type  => 'spot-enlighter',
            label => 'Contactpersoon',
            description =>
                'Zoek hier naar een medewerker die dienst doet als contactpersoon voor de <abbr title="Identity Provider">IdP</abbr>',
            data => {
                restrict    => 'contact/medewerker',
                placeholder => 'Type uw zoekterm',
                label       => 'naam'
            }
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_sp_webservice',
            type     => 'text',
            label    => 'Webdienst base URL',
            required => 1,
            description =>
                'Dit is de URL die gebruikt wordt in de SAML Protocol Exchange. Omdat Zaaksysteem mogelijk vanaf meerdere URLs benaderd kan worden is het van belang er een vast te zetten. Controleer dat het domein in deze URL gelijk is aan het domein waarvoor PKIoverheid certificaten zijn afgegeven',
            data => { placeholder => 'http://mijn.gemeente.nl/auth/saml' }
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_sp_application_name',
            type     => 'text',
            label    => 'Applicatie naam',
            required => 1,
        ),
    );
    return \@fields;
}

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'SAML 2.0 Service Provider',
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 0,
    is_manual                     => 1,
    retry_on_error                => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    test_interface                => 1,
    interface_update_callback => sub {
        return shift->_update_interface_config(@_);
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    $class->$orig(%{ MODULE_SETTINGS() });
};

sub _update_interface_config {
    my ($module, $interface, $model) = @_;

    my $config = $interface->get_interface_config;

    $module->get_certificate_info($interface, $config, qw(sp_cert));

    $interface->update_interface_config($config);
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

