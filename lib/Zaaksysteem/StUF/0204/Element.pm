package Zaaksysteem::StUF::0204::Element;

use Moose;

=head1 NAME

Zaaksysteem::StUF::0204::Element - Element object, to write complex XML elements

=head1 SYNOPSIS

    $ADR->{'postcode'}   = Zaaksysteem::StUF::0204::Element->new(
        novalue => 'waardeOnbekend',
        name    => $stufname,
    );

    ### Turns: <postcode />
    ### into: <postcode xsi:nil="true" StUF:noValue="waardeOnbekend"/>

=head1 DESCRIPTION

Creates a complex element, for instance: for setting attributes on an element.

=head1 ATTRIBUTES

=head1 novalue

Determines the C<noValue> attribute, use one of:

    nietOndersteund
    nietGeautoriseerd
    geenWaarde
    waardeOnbekend

=cut

has 'novalue'   => (
    is      => 'rw',
    isa     => 'Str',
);

=head1 NAME

The name of the element to create

=cut


has 'name'      => (
    is      => 'rw',
    isa     => 'Str',
);


=head1 METHODS

=head2 toXML

Arguments: $value, $doc, $path

Return value: L<XML::LibXML::Element>

    $ADR->{'postcode'}   = Zaaksysteem::StUF::0204::Element->new(
        novalue => 'waardeOnbekend',
        name    => $stufname,
    );

    ### Turns: <postcode />
    ### into: <postcode xsi:nil="true" StUF:noValue="waardeOnbekend"/>

Internal method for a writer below L<XML::Compile>. Will turn this object
into a L<XML::LibXML::Element> object.

=cut

sub toXML {
    my ($self, $value, $doc, $path) = @_;

    my $element = $doc->createElement($self->name);

    my @attrs;
    if ($self->novalue) {
        push(@attrs, $doc->createAttribute('xsi:nil' => 'true'));
        push(@attrs, $doc->createAttribute('StUF:noValue' => $self->novalue));
    } else {
        my $value = $doc->createTextNode($self->value) if defined $self->value;
        $element->addChild($value);
    }

    if (@attrs) {
        $element->addChild($_) for @attrs;
    }

    return $element;

}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
