package Zaaksysteem::Event::Emitter;
use warnings;
use strict;

=head1 NAME

Zaaksysteem::Event::Emitter - Emit events to others

=head1 DESCRIPTION

This module is used for emitting events/notifications to users outside of the
application. It is a sort of counterpart to the rabbitMQ bit of Python. Instead
of listening to an event we get notified by Python to emit an event in Perl.

=head1 SYNOPSIS

=cut

use List::Util qw(first);
use BTTW::Tools qw(throw);
use Module::Pluggable
    require     => 1,
    sub_name    => '_plugins',
    search_path => 'Zaaksysteem::Event::P5',
;


our @PLUGINS;
sub plugins {
    return @PLUGINS if @PLUGINS;
    @PLUGINS = shift->_plugins;
    return @PLUGINS;
}

sub new_from_event {
    my ($self, $event, @args) = @_;

    $self->plugins();

    my $module = first { $_->supports_event($event->{event_name}) } @PLUGINS;
    unless ($module) {
        throw("event/handler/no_configured_event",
            "Unable to handle event with name $event->{event_name}");
    }

    return $module->new(@args, event => $event);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
