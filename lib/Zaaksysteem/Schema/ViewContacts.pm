use utf8;
package Zaaksysteem::Schema::ViewContacts;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ViewContacts

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<view_contacts>

=cut

__PACKAGE__->table("view_contacts");
__PACKAGE__->result_source_instance->view_definition(" SELECT 'person'::text AS type,\n    'natuurlijk_persoon'::text AS legacy_type,\n    natuurlijk_persoon.id,\n    natuurlijk_persoon.uuid,\n    get_display_name_for_person(hstore(ARRAY['voorletters'::text, 'naamgebruik'::text, 'geslachtsnaam'::text], ARRAY[(natuurlijk_persoon.voorletters)::text, ((natuurlijk_persoon.naamgebruik)::character varying)::text, ((natuurlijk_persoon.geslachtsnaam)::character varying)::text])) AS display_name\n   FROM natuurlijk_persoon\nUNION\n SELECT 'organization'::text AS type,\n    'bedrijf'::text AS legacy_type,\n    bedrijf.id,\n    bedrijf.uuid,\n    get_display_name_for_company(hstore(ARRAY['handelsnaam'::text], ARRAY[bedrijf.handelsnaam])) AS display_name\n   FROM bedrijf\nUNION\n SELECT 'employee'::text AS type,\n    'medewerker'::text AS legacy_type,\n    subject.id,\n    subject.uuid,\n    get_display_name_for_employee(hstore(ARRAY['properties'::text], ARRAY[subject.properties])) AS display_name\n   FROM subject\n  WHERE (subject.subject_type = 'employee'::text)");

=head1 ACCESSORS

=head2 type

  data_type: 'text'
  is_nullable: 1

=head2 legacy_type

  data_type: 'text'
  is_nullable: 1

=head2 id

  data_type: 'integer'
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 display_name

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "type",
  { data_type => "text", is_nullable => 1 },
  "legacy_type",
  { data_type => "text", is_nullable => 1 },
  "id",
  { data_type => "integer", is_nullable => 1 },
  "uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "display_name",
  { data_type => "text", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-08-30 14:15:35
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2eUJHNxfpyCIV90hGeAevA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
