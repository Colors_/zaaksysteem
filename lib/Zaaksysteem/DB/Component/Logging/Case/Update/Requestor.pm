package Zaaksysteem::DB::Component::Logging::Case::Update::Requestor;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use JSON;

sub onderwerp {
    my $self = shift;

    if ($self->data->{old_value}) {
        return sprintf(
            'Aanvrager gewijzigd van "%s" naar "%s"',
            $self->data->{old_value},
            $self->data->{new_value},
        );
    }
    return sprintf('Aanvrager ingesteld op "%s"', $self->data->{new_value});
}

=head2 event_category

Type type of event category for this logging item.

=cut

sub event_category { 'case-mutation'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

