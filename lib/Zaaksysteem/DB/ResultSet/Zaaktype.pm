package Zaaksysteem::DB::ResultSet::Zaaktype;

use strict;
use warnings;

use Moose;
use Zaaksysteem::Constants;
use Data::Dumper;

extends 'Zaaksysteem::Backend::ResultSet',
        'Zaaksysteem::Zaaktypen::BaseResultSet';

__PACKAGE__->load_components(qw/Helper::ResultSet::SetOperations/);

# COMPLETE: 100%
use constant    PROFILE => {
    required        => [qw/
        bibliotheek_categorie_id
    /],
    optional        => [qw/
    /],
    msgs            => PARAMS_PROFILE_DEFAULT_MSGS,
};

has '_active_params' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        return {
            'me.active'     => 1,
            'me.deleted'    => undef,
        }
    }
);


### Performance optimalization
sub search_freeform {
    my $self    = shift;

    my $search  = $self->next::method(@_);

    return $search->search({}, { prefetch => 'zaaktype_node_id'});
}

sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(@_, $profile, 1);
}


sub search_with_options {
    my ($self, $args) = @_;

    my $whereClause = {
        'me.deleted' => undef,
    };

    if (defined($args->{trigger}) && $args->{trigger}) {
        $whereClause->{'zaaktype_node_id.trigger'} = {
            'ilike' => '%' . $args->{trigger} .  '%'
        };
    }

    if (defined($args->{betrokkene_type}) && $args->{betrokkene_type}) {
        $whereClause->{'zaaktype_betrokkenens.betrokkene_type'} =
            $args->{betrokkene_type};
    }

    if($args->{term}) {
        my $search_request  = { ilike => '%' .  $args->{term} . '%' };
        $whereClause->{'-or'} = [
            { 'zaaktype_node_id.titel'                 => $search_request },
            { 'zaaktype_node_id.zaaktype_trefwoorden'  => $search_request },
            { 'zaaktype_node_id.zaaktype_omschrijving' => $search_request },
        ];
    }

    my $options = {
        join => [
            'zaaktype_node_id',
            { zaaktype_node_id => 'zaaktype_betrokkenens' },
        ],
        prefetch => [{zaaktype_node_id => 'zaaktype_definitie_id' }],
        group_by => [qw[
            me.id
            me.zaaktype_node_id
            me.version
            me.active
            me.created
            me.last_modified
            me.deleted
            me.bibliotheek_categorie_id
            me.search_index
            me.search_term
            me.object_type
            me.searchable_id
            zaaktype_node_id.id
            zaaktype_node_id.titel
            zaaktype_definitie_id.id
        ]]
    };

    return $self->search($whereClause, $options);
}


=head2 find_by_lowercase_title

Legacy support for url matching. Find a casetype using a lowercase
match on title.

=cut

sub find_by_lowercase_title {
    my ($self, $title) = @_;

    return $self->search({
        'LOWER(zaaktype_node_id.titel)' => $title
    }, {
        join => 'zaaktype_node_id',
        order_by => { -desc => 'zaaktype_node_id.id' }
    })->first;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 PARAMS_PROFILE_DEFAULT_MSGS

TODO: Fix the POD

=cut

=head2 PROFILE

TODO: Fix the POD

=cut

=head2 search_freeform

TODO: Fix the POD

=cut

=head2 search_with_options

TODO: Fix the POD

=cut

