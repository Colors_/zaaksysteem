=head1 NAME

Zaaksysteem::Manual::API::V1::Types::XMLNamespace - Type definition for XML namespace objects

=head1 DESCRIPTION

This page documents the serialization of C<xml_namespace> objects.

=head1 JSON

=begin javascript

{
    "type": "xml_namespace",
    "reference": null,
    "instance": {
        "prefix": "foo",
        "namespace_uri": "urn:namespace:uri"
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 prefix E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Prefix used to use for this XML namespace.

Tends to be used for mapping prefixes used in XPath to the namespaces in the document it's
being used on.

=head2 namespace_uri E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

The XML namespace URI (as used in the C<xmlns> attribute in XML documents) for
this namespace.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
