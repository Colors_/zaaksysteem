package Zaaksysteem::Controller::Beheer::Zaaktypen::Version;

use Moose;
use namespace::autoclean;

use Hash::Merge::Simple qw( clone_merge );
use XML::Simple;

use Zaaksysteem::Constants;

BEGIN { extends 'Zaaksysteem::Controller' }

use constant ZAAKTYPEN              => 'zaaktypen';
use constant ZAAKTYPEN_MODEL        => 'DB::Zaaktype';
use constant CATEGORIES_DB          => 'DB::BibliotheekCategorie';

sub base : Chained('/') : PathPart('beheer/zaaktypen'): CaptureArgs(1) {
    my ($self, $c, $zaaktype_id) = @_;

    $c->stash->{zaaktype_id} = $zaaktype_id;
    $c->stash->{zaaktype} = $c->model('DB::Zaaktype')->find($zaaktype_id)
        or die "couldn't find zaaktype $zaaktype_id";

    $c->stash->{nowrapper} = 1;
}


sub version : Chained('base') : PathPart('version') {
    my ( $self, $c ) = @_;

    $c->stash->{max_rows} = 25;

    my $zaaktype_node = $c->stash->{ zaaktype }->zaaktype_node_id;

    $c->stash->{titel} = $zaaktype_node->titel;
    $c->stash->{current_zaaktype_node_id} = $zaaktype_node->id;

    my $options = {
        order_by => {-desc => 'id'},
    };

    unless($c->req->param('show_all_results')) {
        $options->{rows} = $c->stash->{max_rows};
        $options->{page} = 1;
    }

    $c->stash->{zaaktype_nodes} = $c->model('DB::ZaaktypeNode')->search({
        zaaktype_id => $c->stash->{zaaktype_id},
    }, $options);

    $c->stash->{template} = 'beheer/zaaktypen/version.tt';
}


sub activate : Chained('base') : PathPart('version/activate') : Args() {
    my ( $self, $c, $zaaktype_node_id ) = @_;

    $c->stash->{zaaktype}->set_active_node($zaaktype_node_id);

    my $event = $c->model('DB::Logging')->trigger('casetype/update/version', {
        component => LOGGING_COMPONENT_ZAAKTYPE,
        component_id => $c->stash->{zaaktype_id},
        data => {
            casetype_id => $c->stash->{zaaktype_id},
            version => $zaaktype_node_id,
            reason => $c->req->param('commit_message') // '&lt;Geen reden opgegeven&gt;'
        }
    });

    $c->stash->{nowrapper} = 1;
    $c->stash->{message} = 'Het zaaktype is ingesteld op versie ' .$zaaktype_node_id;
    $c->forward("version");
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CATEGORIES_DB

TODO: Fix the POD

=cut

=head2 LOGGING_COMPONENT_ZAAKTYPE

TODO: Fix the POD

=cut

=head2 ZAAKTYPEN

TODO: Fix the POD

=cut

=head2 ZAAKTYPEN_MODEL

TODO: Fix the POD

=cut

=head2 activate

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 version

TODO: Fix the POD

=cut

