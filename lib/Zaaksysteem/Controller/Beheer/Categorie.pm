package Zaaksysteem::Controller::Beheer::Categorie;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

use constant CATEGORIES_DB          => 'DB::BibliotheekCategorie';

use constant CATEGORIE_DESTINATIONS => {
    'zaaktype_catalogus' => {
        url                 => 'bibliotheek',
        naam                => 'Zaaktype Catalogus',
    },
    'zaaktypen'     => {
        url                 => 'zaaktypen',
        naam                => 'Zaaktype Bibliotheek',
    },
    'kenmerken'     => {
        url                 => 'bibliotheek/kenmerken',
        naam                => 'Kenmerken Bibliotheek',
    },
    'sjablonen'     => {
        url                 => 'bibliotheek/sjablonen',
        naam                => 'Sjablonen Bibliotheek',
    },
};

my $DESTINATIONS    = CATEGORIE_DESTINATIONS;

my $CATEGORY_FIELDS = {
    'naam'      => 'naam',
};


sub base : Chained('/beheer') : PathPart(''): CaptureArgs(0) {
    my ( $self, $c ) = @_;
}


sub categories : Private {
    my ( $self, $c, $catid ) = @_;

    $c->stash->{template}   = 'beheer/categorie/index.tt';

    $c->stash->{dest}       = $DESTINATIONS->{
        $c->stash->{dest_type}
    };

    $c->stash->{cat_url}    = $c->uri_for(
        '/beheer/' . $c->stash->{dest}->{url}
    );

    $c->add_trail(
        {
            uri     => $c->uri_for('/beheer/' .
                $c->stash->{dest}->{url}
            ),
            label   => $c->stash->{dest}->{naam},
        }
    );


    my $categories = $c->model(CATEGORIES_DB)->search({}, {order_by => 'naam'});

    # apply textfilter to results
    my $params = $c->req->params();
    my $textfilter = $params->{'textfilter'};

    if($textfilter) {
        if($c->stash->{'child_category_ids'}) {
            $categories = $categories->search({ 'id' => {'-in' => $c->stash->{'child_category_ids'}}});
        }

        $categories = $categories->search({'naam' => {'ilike' => '%'. $textfilter. '%' }});
    } else {
        $categories = $categories->search({
            pid => (
                $catid && $catid =~ /^\d+$/
                    ? $catid
                    : undef
            ),
        });
    }

    $c->stash->{'categories'} = $categories;


    if ($catid && $catid =~ /^\d+$/) {
        $c->stash->{'categorie'}   = $c->model(CATEGORIES_DB)->find($catid);

        $c->add_trail(
            {
                uri     => $c->uri_for('/beheer/bibliotheek/'
                    . $c->stash->{bib_type} . '/'
                    . $catid
                ),
                label   => 'Categorie: ' . (
                    $c->stash->{'categorie'}
                        ? $c->stash->{'categorie'}->naam
                        : ''
                    ),
            }
        );
    }
}



sub list : Private {
    my ($self, $c) = @_;

    my $categorie_id = $c->stash->{categorie} ? $c->stash->{categorie}->id : undef;
    $c->stash->{entries} = $c->stash->{entries}->search({'bibliotheek_categorie_id' => $categorie_id});
#    $c->stash->{template} = 'widgets/beheer/' . $c->stash->{dest}->{template_list};
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CATEGORIES_DB

TODO: Fix the POD

=cut

=head2 categories

TODO: Fix the POD

=cut

=head2 list

TODO: Fix the POD

=head2 base

TODO: Fix the POD

=cut

