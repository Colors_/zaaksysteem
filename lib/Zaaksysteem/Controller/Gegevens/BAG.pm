package Zaaksysteem::Controller::Gegevens::BAG;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('gegevens/bag'): CaptureArgs(1) {
    my ($self, $c, $bagid) = @_;

    $c->stash->{bag} = $c->model('Gegevens::BAG')->retrieve(
        'id'    => $bagid
    ) or $c->detach;
}

sub info : Chained('base') : PathPart('info'): Args(1) {
    my ($self, $c, $template) = @_;

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'widgets/gegevens/bag/' . $template . '.tt';
}


sub search : Local {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{json_response}) {

        $c->stash->{json} = {
            'entries'    => []
        };

        my $entries = [];
        if ($c->req->params->{searchtype} eq 'openbareruimte') {
            $c->detach unless (
                $c->req->params->{straatnaam}
            );

            my $straatnaam = $c->req->params->{straatnaam};
            $straatnaam =~ s/.+> //;

            my $ors = $c->model('DB::BagOpenbareruimte')->search(
                {
                    'lower(naam)'    => {
                        like    => lc($straatnaam) . '%'
                    }
                },
            );

            while (my $or = $ors->next) {
                push( @{ $entries },
                    {
                        nummeraanduiding => undef,
                        straatnaam       => $or->naam,
                        woonplaats       => $or->woonplaats->naam,
                        identificatie    => 'openbareruimte-' . $or->identificatie
                    },
                );
            }
        } else {
            $c->detach unless (
                (
                    (
                        $c->req->params->{postcode} ||
                        $c->req->params->{straatnaam}
                    ) &&
                    $c->req->params->{huisnummer}
                )
            );
            my $straatnaam = $c->req->params->{straatnaam};
            $straatnaam =~ s/.+> //;

            my $huisnummer  = $c->req->params->{huisnummer};
            $huisnummer =~ s/[^\d]//g;

            $c->detach unless (
                $huisnummer =~ /^\d+$/
            );

            if ( $c->req->params->{postcode} ) {
                my $nas = $c->model('DB::BagNummeraanduiding')->search(
                    {
                        postcode    => uc($c->req->params->{postcode}),
                        huisnummer  => $huisnummer,
                    },
                );

                while (my $na = $nas->next) {
                    push( @{ $entries },
                        {
                            nummeraanduiding => $na->nummeraanduiding,
                            straatnaam       => $na->openbareruimte->naam,
                            woonplaats       => $na->openbareruimte->woonplaats->naam,
                            identificatie    => 'nummeraanduiding-' . $na->identificatie
                        },
                    );
                }
            } else {
                my $or = $c->model('DB::BagOpenbareruimte');

                if ($c->req->params->{woonplaats}) {
                    $or = $c->model('DB::BagWoonplaats')->search(
                        {
                            naam    => $c->req->params->{woonplaats}
                        }
                    )->first->openbareruimten;
                    warn('searched woonplaats');
                }
                my $straten = $or->search(
                    {
                        naam  => $straatnaam
                    },
                );

                my $straat  = $straten->first;

                my $nas     = $straat->hoofdadressen->search(
                    {
                        huisnummer  => $huisnummer,
                    }
                );

                while (my $na = $nas->next) {
                    push( @{ $entries },
                        {
                            nummeraanduiding => $na->nummeraanduiding,
                            straatnaam       => $straat->naam,
                            woonplaats       => $straat->woonplaats->naam,
                            identificatie    => 'nummeraanduiding-' . $na->identificatie
                        },
                    );
                }

            }
        }

        $c->stash->{json} = {
            'entries'    => $entries,
        };
        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }
}

sub search_andere_zaken_met_bag_id : Local {
    my ($self, $c, $bagid) = @_;

    unless ($c->req->is_xhr) {
        $c->detach;
    }

}

sub import_bag : Local {
    my ($self, $c) = @_;

    $c->model('Gegevens::BAG')->import_start;

    $c->res->body('OK');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 import_bag

TODO: Fix the POD

=cut

=head2 info

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 search_andere_zaken_met_bag_id

TODO: Fix the POD

=head2 base

TODO: Fix the POD

=cut

