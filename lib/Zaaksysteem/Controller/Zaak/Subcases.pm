package Zaaksysteem::Controller::Zaak::Subcases;

use Moose;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

use constant SUBCASE_FIELDS => [qw/eigenaar_type kopieren_kenmerken ou_id role_id required/];

sub edit : Chained('/zaak/base') : PathPart('subcase') : Args() {
    my ($self, $c, $zaaktype_relatie_id) = @_;

    my $zaaktype_relatie = $c->model('DB::ZaaktypeRelatie')->find($zaaktype_relatie_id);
    die "zaaktype_relatie_id with id $zaaktype_relatie_id not found" unless($zaaktype_relatie);

    my $relatie_params = { $zaaktype_relatie->get_columns };

    $c->stash->{params} = $relatie_params;

    $c->stash->{submit_button} = 'Starten';
    $c->stash->{template}  = 'beheer/zaaktypen/milestones/edit_relatie.tt';
    $c->stash->{nowrapper} = 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 edit

TODO: Fix the POD

=cut

