BEGIN;

  DROP VIEW IF EXISTS case_acl;

  CREATE VIEW case_acl AS
  SELECT
    z.id AS case_id,
    z.uuid AS case_uuid,
    cam.key AS permission,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id as casetype_id
  FROM
    zaak z
    JOIN zaaktype_authorisation za on (za.zaaktype_id = z.zaaktype_id and za.confidential = z.confidential)
    JOIN subject_position_matrix spm
    JOIN subject s ON (spm.subject_id = s.id) ON (spm.role_id = za.role_id AND spm.group_id = za.ou_id)
    JOIN case_authorisation_map cam ON (za.recht = cam.legacy_key)
  UNION ALL
  SELECT
    z.id,
    z.uuid,
    za.capability,
    s.id,
    s.uuid,
    z.zaaktype_id
  FROM
    zaak z
    JOIN zaak_authorisation za ON (za.zaak_id = z.id
        AND za.entity_type = 'position')
    JOIN subject_position_matrix spm
    JOIN subject s ON (spm.subject_id = s.id) ON spm.position = za.entity_id
  UNION ALL
  SELECT
    z.id,
    z.uuid,
    za.capability,
    s.id,
    s.uuid,
    z.zaaktype_id
  FROM
    zaak z
    JOIN zaak_authorisation za ON (za.zaak_id = z.id
        AND za.entity_type = 'user')
    JOIN subject s ON (s.username = za.entity_id)
  UNION ALL
  SELECT
    z.id AS case_id,
    z.uuid AS case_uuid,
    unnest(ARRAY['read', 'write', 'search'])::text,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id as casetype_id
  FROM
    zaak z
    JOIN zaak_betrokkenen zb ON (
      zb.id = z.aanvrager
      OR
      zb.id = z.behandelaar
      OR
      zb.id = z.coordinator
      AND zb.betrokkene_type = 'medewerker'
    )
    JOIN subject s ON (zb.betrokkene_id = s.id)
   ;

COMMIT;
