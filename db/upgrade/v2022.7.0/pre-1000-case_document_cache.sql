BEGIN;

  DROP VIEW IF EXISTS case_documents CASCADE;
  CREATE VIEW case_documents AS
   SELECT z.id AS case_id,
    ARRAY( SELECT f.id::character varying AS id
           FROM file_case_document fcd
           JOIN file f
           ON f.id = fcd.file_id
           WHERE fcd.case_id = z.id and bk.bibliotheek_kenmerken_id = fcd.bibliotheek_kenmerken_id)::text[] AS value,
    bk.magic_string,
    bk.bibliotheek_kenmerken_id AS library_id
   FROM zaak z
     JOIN zaaktype_document_kenmerken_map bk
     ON z.zaaktype_node_id = bk.zaaktype_node_id
   ;

  CREATE OR REPLACE FUNCTION attribute_file_value_to_v0_jsonb(
    IN value text[],
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    length int;
    m text;

    r record;

  BEGIN

    value_json := '[]'::jsonb;

    FOR r IN
      SELECT
        f.accepted as accepted,
        f.confidential as confidential,
        CONCAT(f.name, f.extension) as filename,
        f.id as id,
        fs.uuid as uuid,
        fs.size as filesize,
        fs.original_name as original_name,
        fs.mimetype as mimetype,
        fs.md5 as md5,
        fs.is_archivable as is_archivable
      FROM
        filestore fs
      JOIN
        file f
      ON
        (f.filestore_id = fs.id)
      WHERE
        f.id = ANY(value::int[])
      ORDER by f.id
    LOOP
      select into value_json value_json || jsonb_build_object(
        'accepted', CASE WHEN r.accepted = true THEN 1 ELSE 0 END,
        'confidential', r.confidential,
        'file_id', r.id,
        'filename', r.filename,
        'is_archivable', CASE WHEN r.is_archivable = true THEN 1 ELSE 0 END,
        'md5', r.md5,
        'mimetype', r.mimetype,
        'original_name', r.original_name,
        'size', r.filesize,
        'thumbnail_uuid', null,
        'uuid', r.uuid
      );
    END LOOP;
    RETURN;
  END;
  $$;

   CREATE OR REPLACE FUNCTION attribute_value_to_jsonb(
    IN value text[],
    IN value_type text,
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    length int;
    m text;

  BEGIN

      value_json := '[]'::jsonb;
      SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));

      IF length = 0 and value_type = 'file'
      THEN
        value_json := '[]'::jsonb;
        RETURN;
      ELSIF length = 0
      THEN
        value_json := '[null]'::jsonb;
        RETURN;
      END IF;

      IF value_type = 'date'
      THEN
        SELECT INTO value array_agg(attribute_date_value_to_text(value[1]));
      END IF;

      IF value_type LIKE 'bag_%'
      THEN
        SELECT INTO value_json bag_attribute_value_to_jsonb(value, value_type);
        RETURN;
      END IF;

      IF value_type IN ('geojson', 'address_v2', 'appointment_v2', 'relationship')
      THEN

          FOREACH m IN ARRAY value
          LOOP
            SELECT INTO value_json value_json || jsonb_build_array((CONCAT('[', m::text, ']')::json ->> 0)::json);
          END LOOP;

      ELSIF value_type IN ('checkbox', 'select')
      THEN
        SELECT INTO value_json to_jsonb(ARRAY[value]);
      ELSE
        SELECT INTO value_json to_jsonb(value);
      END IF;

      IF value_type = 'numeric'
      THEN
        value_json := '[]'::jsonb;
        FOREACH m IN ARRAY value
        LOOP
          SELECT INTO value_json value_json || to_jsonb(m::numeric::bigint);
        END LOOP;
        RETURN;
      END IF;

      IF value_type = 'file'
      THEN
        SELECT INTO value ARRAY(SELECT fs.uuid::character varying AS uuid FROM filestore fs JOIN file f ON f.filestore_id = fs.id WHERE f.id = ANY(value::int[]))::text[];
        SELECT INTO value_json to_jsonb(value);
      END IF;

      RETURN;
    END;
    $$;

COMMIT;
