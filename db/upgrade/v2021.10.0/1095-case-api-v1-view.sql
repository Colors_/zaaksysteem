BEGIN;

  CREATE OR REPLACE FUNCTION municipality_code_to_json(
    IN unit hstore,
    OUT unit_json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  BEGIN
      SELECT INTO unit_json json_build_object(
        'reference', unit->'uuid',
        'preview', unit->'label',
        'type', 'municipality_code',
        'instance', json_build_object(
          'label', unit->'label',
          'dutch_code', lpad(unit->'dutch_code', 4, '0'),
          'alternative_name', unit->'alternative_name',
          'date_created', NOW(),
          'date_modified', NOW()
        )
      );
      RETURN;
  END;
  $$;

  DROP VIEW IF EXISTS municipality_code_v1_view CASCADE;

  CREATE VIEW municipality_code_v1_view AS
    SELECT r.id, r.dutch_code, r.uuid, r.label, f as json
    FROM municipality_code r,
    LATERAL municipality_code_to_json(hstore(r)) f;

  CREATE OR REPLACE FUNCTION country_code_to_json(
    IN unit hstore,
    OUT unit_json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  BEGIN
      SELECT INTO unit_json json_build_object(
        'reference', unit->'uuid',
        'preview', unit->'label',
        'type', 'country_code',
        'instance', json_build_object(
          'label', unit->'label',
          'alpha_one', null,
          'alpha_two', null,
          'alpha_three', null,
          'code', null,
          'dutch_code', lpad(unit->'dutch_code', 4, '0')
        )
      );
      RETURN;
  END;
  $$;

  DROP VIEW IF EXISTS country_code_v1_view CASCADE;

  CREATE VIEW country_code_v1_view AS
    SELECT r.id, r.dutch_code, r.uuid, r.label, f as json
    FROM country_code r,
    LATERAL country_code_to_json(hstore(r)) f;

  CREATE OR REPLACE FUNCTION org_unit(
    IN org hstore,
    IN type TEXT,
    OUT role_json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  BEGIN
      SELECT INTO role_json json_build_object(
        'reference', org->'uuid',
        'preview', org->'name',
        'type', type,
        'instance', json_build_object(
          'name', org->'name',
          'org_id', (org->'id')::int,
          'description', org->'description',
          'system_org', org->'system_org',
          'date_modified', (org->'date_modified')::timestamp with time zone,
          'date_created', (org->'date_created')::timestamp with time zone
        )
      );
      RETURN;
  END;
  $$;

  DROP VIEW IF EXISTS role_v1_view CASCADE;

  CREATE VIEW role_v1_view AS
    SELECT r.id, r.uuid, r.name, f as json
    FROM roles r,
    LATERAL org_unit(hstore(r), 'role') f;

  CREATE OR REPLACE FUNCTION org_unit(
    IN org hstore,
    IN type TEXT,
    OUT role_json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  BEGIN
      SELECT INTO role_json json_build_object(
        'reference', org->'uuid',
        'preview', org->'name',
        'type', type,
        'instance', json_build_object(
          'name', org->'name',
          'org_id', (org->'id')::int,
          'description', org->'description',
          'system_org', org->'system_org',
          'date_modified', (org->'date_modified')::timestamp with time zone,
          'date_created', (org->'date_created')::timestamp with time zone
        )
      );
      RETURN;
  END;
  $$;

  DROP VIEW IF EXISTS role_v1_view CASCADE;

  CREATE VIEW role_v1_view AS
    SELECT r.id, r.uuid, r.name, f as json
    FROM roles r,
    LATERAL org_unit(hstore(r), 'role') f;

  DROP VIEW IF EXISTS group_v1_view CASCADE;

  CREATE VIEW group_v1_view AS
    SELECT r.id, r.uuid, r.name, f as json
    FROM groups r,
    LATERAL org_unit(hstore(r), 'group') f;

  CREATE OR REPLACE FUNCTION legal_entity_code_to_json(
    IN unit hstore,
    OUT unit_json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  BEGIN
      SELECT INTO unit_json json_build_object(
        'reference', unit->'uuid',
        'preview', unit->'label',
        'type', 'legal_entity_type',
        'instance', json_build_object(
          'label', unit->'label',
          'code', unit->'code',
          'active', (unit->'active')::boolean,
          'description', null
        )
      );
      RETURN;
  END;
  $$;

  DROP VIEW IF EXISTS legal_entity_v1_view CASCADE;

  CREATE VIEW legal_entity_v1_view AS
    SELECT r.id, r.code, r.uuid, r.label, f as json
    FROM legal_entity_type r,
    LATERAL legal_entity_code_to_json(hstore(r)) f;

  CREATE OR REPLACE FUNCTION position_matrix(
    IN groups int[],
    IN roles int[],
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    role_id int;
    group_id int;

    gr jsonb;
    role json;

    pos jsonb;
  BEGIN

      pos := '[]'::jsonb; -- || does nothing when value is NULL

      FOREACH group_id IN ARRAY groups
      LOOP
        SELECT INTO gr gv.json FROM group_v1_view gv WHERE id = group_id;
        FOREACH role_id IN ARRAY roles
        LOOP
          SELECT INTO role gv.json FROM group_v1_view gv WHERE id = group_id;
          SELECT INTO pos pos || jsonb_build_object(
            'preview', 'position(unsynched)',
            'reference', NULL,
            'type', 'position/route',
            'instance', json_build_object(
              'date_modified', NOW(),
              'date_created', NOW(),
              'group', gr::jsonb,
              'role', role::jsonb
            )
          );
        END LOOP;
      END LOOP;

      SELECT INTO json json_build_object(
        'type', 'set',
        'reference', null,
        'instance', json_build_object(
          'pager', null,
          'rows', pos
        )
      );
  END;
  $$;


  CREATE OR REPLACE FUNCTION subject_employee_json(
    IN subject hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    properties jsonb;
    preview text;
    positions jsonb;

  BEGIN
      properties := (subject->'properties')::jsonb;

      SELECT INTO json subject_json(subject, 'employee');

      SELECT INTO positions position_matrix(
        (subject->'group_ids')::int[],
        (subject->'role_ids')::int[]
      );

      SELECT INTO json json || jsonb_build_object(
        'instance', json_build_object(
          'username', subject->'username',
          'initials', properties->'initials',
          'first_names', properties->'givenname',
          'surname', properties->'surname',
          'display_name', properties->'displayname',
          'email_address', properties->'mail',
          'phone_number', properties->'telephonenumber',
          'settings', (subject->'settings')::jsonb,
          'positions', positions,
          'date_modified', (subject->'last_modified')::timestamp with time zone,
          'date_created', null
        )
      );
      RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION subject_json(
    IN subject hstore,
    IN s_type TEXT,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    preview text;
  BEGIN
      -- uuid == 36 long, need last 6 chars, 31, 36
      SELECT INTO preview substring(subject->'uuid', 31, 36);

      SELECT INTO json jsonb_build_object(
        'reference', subject->'uuid',
        'preview', s_type || '(...' || preview || ')',
        'type', s_type
      );
      RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION subject_person_json(
    IN subject hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE

    address json;
    address_cor json;
    address_res json;

  BEGIN

    SELECT INTO json subject_json(subject, 'person');

    address := json_build_object(
      'preview', 'address(unsynched)',
      'reference', null,
      'type', 'address',
      'instance', json_build_object(
        'bag_id', subject->'bag_id',
        'city', subject->'city',
        'street', subject->'street',
        'street_number', (subject->'street_number')::int,
        'street_number_suffix', subject->'street_number_suffix',
        'street_number_letter', subject->'street_number_letter',
        'foreign_address_line1', subject->'foreign_address_line1',
        'foreign_address_line2', subject->'foreign_address_line2',
        'foreign_address_line3', subject->'foreign_address_line3',
        'zipcode', subject->'zipcode',
        'country', (subject->'country')::jsonb,
        'municipality', (subject->'municipality')::jsonb,
        'latitude', (subject->'latitude')::float,
        'longitude', (subject->'longitude')::float
      )
    );

    IF subject->'address_type' = 'W' THEN
          address_res := address;
    ELSE
          address_cor := address;
    END IF;

    SELECT INTO json json || jsonb_build_object(
      'instance', json_build_object(
        'address_residence', address_res,
        'address_correspondence', address_cor,
        'email_address', subject->'email',
        'phone_number', subject->'phone_number',
        'mobile_phone_number', subject->'mobile',
        'partner', null,
        -- non-existent data
        'date_created', NOW(), -- np table?
        'date_modified', NOW(), -- np table?
        -- we have it
        'date_of_birth', (subject->'geboortedatum')::date,
        'date_of_death', (subject->'datum_overlijden')::date,
        'family_name', subject->'geslachtsnaam',
        'first_names', subject->'voornamen',
        'gender', subject->'geslachtsaanduiding',
        'initial', subject->'voorletters',
        'is_local_resident', (subject->'in_gemeente')::boolean,
        'is_secret', COALESCE((subject->'indicatie_geheim')::int, 0),
        'noble_title', subject->'adellijke_titel',
        'personal_number', lpad(subject->'burgerservicenummer', 9, '0'),
        'personal_number_a', lpad(subject->'a_nummer', 10, '0'),
        'place_of_birth', subject->'geboorteplaats',
        'prefix', subject->'aanhef_aanschrijving',
        'surname', COALESCE(subject->'naamgebruik', subject->'geslachtsnaam'),
        'use_of_name', subject->'aanduiding_naamgebruik'
      )
    );

    RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION subject_company_json(
    IN subject hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE

    address_cor json;
    address_res json;

    latitude float;
    longitude float;

  BEGIN

    SELECT INTO json subject_json(subject, 'company');

    latitude := ((subject->'vestiging_latlong')::point)[0];
    longitude := ((subject->'vestiging_latlong')::point)[1];

    IF (
        subject->'vestiging_adres_buitenland1' IS NOT NULL
        AND
        subject->'vestiging_adres_buitenland2' IS NOT NULL
        AND
        subject->'vestiging_adres_buitenland3' IS NOT NULL
        )
      OR (subject->'vestiging_straatnaam' IS NOT NULL
          AND subject->'vestiging_postcode' IS NOT NULL
      )
    THEN
      address_res := json_build_object(
        'preview', 'address(unsynched)',
        'reference', null,
        'type', 'address',
        'instance', json_build_object(
          'bag_id', (subject->'vestiging_bag_id')::bigint,
          'city', subject->'vestiging_woonplaats',
          'street', subject->'vestiging_straatnaam',
          'street_number', (subject->'vestiging_huisnummer')::int,
          'street_number_suffix', subject->'vestiging_huisnummer_toevoeging',
          'street_number_letter', subject->'vestiging_huisletter',
          'foreign_address_line1', subject->'vestiging_adres_buitenland1',
          'foreign_address_line2', subject->'vestiging_adres_buitenland2',
          'foreign_address_line3', subject->'vestiging_adres_buitenland3',
          'country', (subject->'vestiging_country')::jsonb,
          'zipcode', subject->'vestiging_postcode',
          'latitude', latitude,
          'longitude', longitude
        )
      );
    END IF;

    IF (
        subject->'correspondentie_adres_buitenland1' IS NOT NULL
        AND
        subject->'correspondentie_adres_buitenland2' IS NOT NULL
        AND
        subject->'correspondentie_adres_buitenland3' IS NOT NULL
        )
      OR (subject->'correspondentie_straatnaam' IS NOT NULL
          AND subject->'correspondentie_postcode' IS NOT NULL
      )
    THEN
      address_cor := json_build_object(
        'preview', 'address(unsynched)',
        'reference', null,
        'type', 'address',
        'instance', json_build_object(
          'bag_id', (subject->'correspondentie_bag_id')::bigint,
          'city', subject->'correspondentie_woonplaats',
          'street', subject->'correspondentie_straatnaam',
          'street_number', (subject->'correspondentie_huisnummer')::int,
          'street_number_suffix', subject->'correspondentie_huisnummer_toevoeging',
          'street_number_letter', subject->'correspondentie_huisletter',
          'foreign_address_line1', subject->'correspondentie_adres_buitenland1',
          'foreign_address_line2', subject->'correspondentie_adres_buitenland2',
          'foreign_address_line3', subject->'correspondentie_adres_buitenland3',
          'zipcode', subject->'correspondentie_postcode',
          'country', (subject->'correspondentie_country')::jsonb,
          'latitude', null,
          'longitude', null
        )
      );
    END IF;

    SELECT INTO json json || jsonb_build_object(
      'instance', json_build_object(
        'address_residence', address_res,
        'address_correspondence', address_cor,
        'email_address', subject->'email',
        'phone_number', subject->'phone_number',
        'mobile_phone_number', subject->'mobile',
        -- non-existent data
        'date_created', NOW(),
        'date_modified', NOW(),
        -- we have it
        'coc_number', lpad(subject->'dossiernummer', 8, '0'),
        'coc_location_number', lpad(subject->'vestigingsnummer', 12, '0'),
        'date_ceased', (subject->'date_ceased')::date,
        'date_founded', (subject->'date_founded')::date,
        'date_registration', (subject->'date_registration')::date,
        'main_activity', (subject->'main_activity')::jsonb,
        'secondairy_activities', (subject->'secondairy_activities')::jsonb,
        'company_type', (subject->'company_type')::jsonb,
        'oin', (subject->'oin')::bigint,
        'rsin', (subject->'rsin')::bigint
      )
    );

    RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION get_display_name_for_person(
    IN np hstore,
    OUT display_name TEXT
  )
  LANGUAGE plpgsql AS $$
  DECLARE
    initials text;
    surname text;
  BEGIN

    initials := np->'voorletters';
    surname := np->'naamgebruik';

    IF surname IS NULL
    THEN
        surname := np->'geslachtsnaam';
    END IF;

    IF initials = '' OR initials IS NULL
    THEN
      display_name := surname;
    ELSE
      display_name := concat(initials, ' ', surname);
    END IF;

  END;
  $$;

  CREATE OR REPLACE FUNCTION get_display_name_for_company(
    IN nnp hstore,
    OUT display_name TEXT
  )
  LANGUAGE plpgsql AS $$
  DECLARE
  BEGIN

    display_name := nnp->'handelsnaam';
    RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION get_display_name_for_employee(
    IN emp hstore,
    OUT display_name TEXT
  )
  LANGUAGE plpgsql AS $$
  DECLARE
  BEGIN

    display_name := (emp->'properties')::jsonb->>'displayname';
    RETURN;
  END;
  $$;

    CREATE OR REPLACE FUNCTION get_subject_display_name_by_uuid(
    IN subject_uuid uuid,
    OUT display_name TEXT
  ) LANGUAGE plpgsql AS $$
    DECLARE
      found record;
      initials text;
      surname text;
    BEGIN
      SELECT INTO found * FROM natuurlijk_persoon where uuid = subject_uuid;

      IF found.uuid IS NOT NULL
      THEN

        SELECT INTO display_name get_display_name_for_person(hstore(found));
        RETURN;
      END IF;

      SELECT INTO found * FROM bedrijf where uuid = subject_uuid;
      IF found.uuid IS NOT NULL THEN
        SELECT INTO display_name get_display_name_for_company(hstore(found));
        RETURN;
      END IF;

      SELECT INTO found * FROM subject where uuid = subject_uuid
        AND subject.subject_type = 'employee';

      IF found.uuid IS NOT NULL THEN
        SELECT INTO display_name get_display_name_for_employee(hstore(found));
        RETURN;
      END IF;

      RETURN;
    END $$;

  CREATE OR REPLACE FUNCTION case_subject_json(
    IN zb hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    btype text;
    gm_id int;
    old_id text;

    subject record;
    s_hstore hstore;
    subject_json json;

    subject_uuid uuid;
    subject_type text;
    display_name text;

  BEGIN

    btype := zb->'betrokkene_type';

    IF btype IS NULL THEN
      json := null;
      RETURN;
    END IF;

    IF btype NOT IN ('medewerker', 'natuurlijk_persoon', 'bedrijf') THEN
      RAISE EXCEPTION 'Unknown betrokkene type %', btype;
    END IF;


    gm_id := (zb->'gegevens_magazijn_id')::int;
    old_id := 'betrokkene-' || btype || '-' || gm_id;

    IF btype = 'medewerker' THEN
      subject_type := 'employee';

      SELECT INTO subject * FROM subject s WHERE s.subject_type = 'employee'
        AND id = gm_id;

      s_hstore := hstore(subject);
      SELECT INTO subject_json subject_employee_json(s_hstore);
      SELECT INTO display_name get_display_name_for_employee(s_hstore);

    ELSIF btype = 'natuurlijk_persoon' THEN

      subject_type := 'person';

      SELECT INTO
        subject
        np.*,
        a.straatnaam as street,
        a.huisnummer as street_number,
        a.huisletter as street_letter,
        a.huisnummertoevoeging as street_number_suffix,
        a.postcode as zipcode,
        a.woonplaats as city,
        a.functie_adres as address_type,
        mc.json as municipality,
        cc.json as country,
        a.adres_buitenland1 as foreign_address_line_1,
        a.adres_buitenland2 as foreign_address_line_2,
        a.adres_buitenland3 as foreign_address_line_3,
        a.bag_id as bag_id,
        a.geo_lat_long[0] as latitude,
        a.geo_lat_long[1] as longitude,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone,
        cd.email as email
      FROM
        natuurlijk_persoon np
      LEFT JOIN
        adres a
      ON
        np.adres_id = a.id
      LEFT JOIN
        country_code_v1_view cc
      ON
        cc.dutch_code = a.landcode
      LEFT JOIN
        municipality_code_v1_view mc
      ON
        mc.dutch_code = a.gemeente_code
      LEFT JOIN
        contact_data cd
      ON
        -- 1 or 2 is much clearer than natuurlijk_persoon or bedrijf ey
        -- :/
        (cd.gegevens_magazijn_id = np.id and cd.betrokkene_type = 1)

      WHERE
        np.id = gm_id;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_person_json(s_hstore);
      SELECT INTO display_name get_display_name_for_person(s_hstore);

    ELSIF btype = 'bedrijf' THEN

      subject_type := 'company';

      SELECT INTO subject
      b.*,
      cc_vestiging.json as vestiging_country,
      cc_correspondentie.json as correspondentie_country,
      let.json as company_type
      FROM
        bedrijf b
      LEFT JOIN
        country_code_v1_view cc_vestiging
      ON
        cc_vestiging.dutch_code = b.vestiging_landcode
      LEFT JOIN
        country_code_v1_view cc_correspondentie
      ON
        cc_correspondentie.dutch_code = b.correspondentie_landcode
      LEFT JOIN
        legal_entity_v1_view let
      ON
        let.code = b.rechtsvorm
      WHERE
        b.id = gm_id
      ;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_company_json(s_hstore);
      SELECT INTO display_name get_display_name_for_company(s_hstore);

    END IF;

    SELECT INTO json json_build_object(
        'preview', display_name,
        'type', 'subject',
        'reference', subject.uuid,
        'instance', json_build_object(
          'date_created', NOW(),
          'date_modified', NOW(),
          'display_name', display_name,
          'external_subscription', null,
          'old_subject_identifier', old_id,
          'subject_type', subject_type,
          'subject', subject_json
        )
    );

  END;
  $$;

  DROP VIEW IF EXISTS case_v1_subjects;

  CREATE VIEW case_v1_subjects AS
    SELECT
      z.id as case_id,
      'requestor' as type,
      ( SELECT case_subject_json(hstore(zb)) ) AS subject
    FROM
      zaak z
    LEFT JOIN
      zaak_betrokkenen zb
    ON
      (zb.zaak_id = z.id and z.aanvrager = zb.id)
    WHERE
      zb.deleted IS NULL
  UNION ALL
    SELECT
      z.id as case_id,
      'assignee' as type,
      ( SELECT case_subject_json(hstore(zb)) ) AS subject
    FROM
      zaak z
    LEFT JOIN
      zaak_betrokkenen zb
    ON
      (zb.zaak_id = z.id and z.behandelaar = zb.id)
    WHERE
      zb.deleted IS NULL
  UNION ALL
    SELECT
      z.id as case_id,
      'coordinator' as type,
      ( SELECT case_subject_json(hstore(zb)) ) AS subject
    FROM
      zaak z
    LEFT JOIN
      zaak_betrokkenen zb
    ON
      (zb.zaak_id = z.id and z.coordinator = zb.id)
    WHERE
      zb.deleted IS NULL

  ;

COMMIT;

BEGIN;

  DROP VIEW IF EXISTS case_v1;

  CREATE VIEW case_v1 AS
  SELECT

    z.id AS number,
    z.uuid AS id,
    z.pid AS number_parent,
    z.number_master AS number_master,
    z.vervolg_van AS number_previous,

    z.onderwerp AS subject,
    z.onderwerp_extern AS subject_external,

    z.status AS status,

    z.created AS date_created,
    z.last_modified AS date_modified,
    z.vernietigingsdatum AS date_destruction,
    z.afhandeldatum AS date_of_completion,
    z.registratiedatum AS date_of_registration,
    z.streefafhandeldatum AS date_target,

    z.payment_status AS payment_status,
    z.payment_amount AS price,

    z.contactkanaal AS channel_of_contact,
    z.archival_state AS archival_state,

    CASE WHEN z.status = 'stalled' THEN
      zm.stalled_since
    ELSE
      NULL
    END AS stalled_since,

    CASE WHEN z.status = 'stalled' THEN
      z.stalled_until
    ELSE
      NULL
    END AS stalled_until,

    zm.current_deadline AS current_deadline,
    zm.deadline_timeline AS deadline_timeline,

    jsonb_object_agg(ca.magic_string, ca.value) AS attributes,

    ztr.id AS result_id,
    ztr.resultaat as result,
    ztr.selectielijst as active_selection_list,

    CASE WHEN (ztr.id IS NOT NULL) THEN
      json_build_object(
        'reference', NULL,
        'type', 'case/result',
        'preview', CASE WHEN ztr.label IS NOT NULL THEN
          ztr.label
        ELSE
          ztr.resultaat
        END,
        'instance', json_build_object(
          'date_created', ztr.created,
          'date_modified', ztr.last_modified,
          'archival_type', ztr.archiefnominatie,
          'dossier_type', ztr.dossiertype,
          'name', CASE WHEN ztr.label IS NOT NULL THEN
            ztr.label
          ELSE
            ztr.resultaat
          END,
          'result', ztr.resultaat,
          'retention_period', ztr.bewaartermijn,
          'selection_list', CASE WHEN ztr.selectielijst = '' THEN
                              NULL
                            ELSE
                              ztr.selectielijst
                            END,
          'selection_list_start', ztr.selectielijst_brondatum,
          'selection_list_end', ztr.selectielijst_einddatum
        )
      )
    ELSE
      NULL
    END AS outcome,

    json_build_object(
      'preview', ct_ref.title,
      'reference', ct_ref.uuid,
      'instance', json_build_object(
        'version', ct_ref.version,
        'name', ct_ref.title
      ),
      'type', 'casetype'
    ) AS casetype,

    json_build_object(
      'preview', gr.name || ', ' || role.name,
      'reference', NULL,
      'type', 'case/route',
      'instance', json_build_object(
        'date_modified', NOW(),
        'date_created', NOW(),
        'group', gr.json,
        'role', role.json
      )
    ) AS route,

    CASE WHEN z.status = 'stalled' THEN
      zm.opschorten
    ELSE
      NULL
    END AS suspension_rationale,

    CASE WHEN z.status = 'resolved' THEN
      zm.afhandeling
    ELSE
      NULL
    END AS premature_completion_rationale,

    zts.fase::text AS phase,

    json_build_object(
      'preview', zts.fase,
      'reference', null,
      'type', 'case/milestone',
      'instance', json_build_object(
        'date_created', NOW(),
        'date_modified', NOW(),
          'phase_label',
            CASE WHEN zts.id IS NOT NULL THEN
              zts.naam
            ELSE
              zts_end.naam
            END,
          'phase_sequence_number',
            CASE WHEN zts.id IS NOT NULL THEN
              zts.status
            ELSE
              zts_end.status
            END,

        'milestone_label', zts_previous.naam,
        'milestone_sequence_number', zts_previous.status,
        'last_sequence', zts_end.status
      )
    ) as milestone,

    json_build_object(
      'type', 'set',
      'instance', json_build_object(
        'rows', COALESCE(crp.relationship, '[]'::jsonb)
      )
    ) AS relations,

    json_build_object(
      'parent', z.pid,
      'continuation', json_build_object(
        'type', 'set',
        'instance', json_build_object(
          'rows', COALESCE(continuation.relationship, '[]'::jsonb)
        )
      ),
      'child', json_build_object(
        'type', 'set',
        'instance', json_build_object(
          'rows', COALESCE(children.relationship, '[]'::jsonb)
        )
      ),
      'plain', json_build_object(
        'type', 'set',
        'instance', json_build_object(
          'rows', COALESCE(crp.relationship, '[]'::jsonb)
        )
      )
    ) AS case_relationships,

    -- Subject API/v1 shit, mind boggling
    requestor.subject::jsonb AS requestor,
    assignee.subject::jsonb AS assignee,
    coordinator.subject::jsonb AS coordinator,

    -- static values
    'Dossier' AS aggregation_scope,

    -- Not available via api/v1
    null AS case_location,
    null AS correspondence_location

  FROM zaak z

  JOIN zaak_meta zm
  ON zm.zaak_id = z.id

  JOIN case_attributes ca
  ON  ca.case_id = z.id

  JOIN casetype_v1_reference ct_ref
  ON z.zaaktype_node_id = ct_ref.casetype_node_id

  LEFT JOIN zaaktype_resultaten ztr
  ON z.resultaat_id = ztr.id

  JOIN group_v1_view gr
  ON (z.route_ou = gr.id)

  JOIN role_v1_view role
  ON (z.route_role = role.id)

  LEFT JOIN zaaktype_status zts
  ON ( z.zaaktype_node_id = zts.zaaktype_node_id AND zts.status = z.milestone + 1)

  LEFT JOIN zaaktype_status zts_previous
  ON ( z.zaaktype_node_id = zts_previous.zaaktype_node_id AND zts_previous.status = z.milestone)

  JOIN casetype_end_status zts_end
  ON z.zaaktype_node_id = zts_end.zaaktype_node_id

  LEFT JOIN case_relationship_json_view crp
  ON ( z.id = crp.case_id and crp.type = 'plain')

  LEFT JOIN case_relationship_json_view continuation
  ON ( z.id = continuation.case_id and continuation.type = 'initiator')

  LEFT JOIN case_relationship_json_view children
  ON ( z.id = children.case_id and children.type = 'parent')

  JOIN case_v1_subjects requestor
  ON (z.id = requestor.case_id and requestor.type = 'requestor')

  LEFT JOIN case_v1_subjects coordinator
  ON (z.id = coordinator.case_id and coordinator.type = 'coordinator')

  LEFT JOIN case_v1_subjects assignee
  ON (z.id = assignee.case_id and assignee.type = 'assignee')

  WHERE z.deleted IS NULL

  GROUP BY
    z.id,
    zm.stalled_since,
    zm.current_deadline,
    zm.deadline_timeline,
    zm.opschorten,
    zm.afhandeling,
    ct_ref.title,
    ct_ref.uuid,
    ct_ref.version,
    role.name,
    role.json::jsonb,
    gr.name,
    gr.json::jsonb,
    ztr.id,
    zts.id,
    zts_previous.id,
    zts_end.status,
    zts_end.naam,
    crp.relationship::jsonb,
    continuation.relationship::jsonb,
    children.relationship::jsonb,
    requestor.subject::jsonb,
    coordinator.subject::jsonb,
    assignee.subject::jsonb
  ;

COMMIT;

