
BEGIN;

  ALTER TABLE file_case_document DROP COLUMN bibliotheek_kenmerken_id;
  ALTER TABLE file_case_document DROP COLUMN case_id;

COMMIT;

