BEGIN;

  CREATE OR REPLACE FUNCTION update_subject_json() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$

    BEGIN

      IF NEW.coordinator IS NOT NULL THEN
        SELECT INTO
          NEW.coordinator_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          zb.id = NEW.coordinator;
      END IF;
      RETURN NEW;

    END;
  $$;

  UPDATE zaak SET uuid = uuid
    WHERE coordinator_v1_json IS NULL
    AND coordinator IS NOT NULL;

  CREATE OR REPLACE FUNCTION update_subject_json() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$

    BEGIN

      IF TG_OP = 'INSERT'
      THEN
        SELECT INTO
          NEW.requestor_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          -- zb does not have the zaak id yet on create..
          zb.id = NEW.aanvrager;

      ELSIF TG_OP = 'UPDATE' AND (NEW.requestor_v1_json IS NULL OR NEW.aanvrager != OLD.aanvrager)
      THEN
        SELECT INTO
          NEW.requestor_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          -- zb does not have the zaak id yet on update, this is handled
          -- later on..
           zb.id = NEW.aanvrager;

      END IF;

      IF NEW.behandelaar IS NOT NULL
      THEN
        IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND ( OLD.behandelaar IS NULL OR NEW.behandelaar != OLD.behandelaar))
        THEN
          SELECT INTO
            NEW.assignee_v1_json
            case_subject_json(hstore(zb))
          FROM
            zaak_betrokkenen zb
          WHERE
            zb.id = NEW.behandelaar;
        END IF;
      ELSE
        NEW.assignee_v1_json := NULL;
      END IF;


      IF NEW.coordinator IS NOT NULL
      THEN
        IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND ( OLD.coordinator IS NULL OR NEW.coordinator != OLD.coordinator))
        THEN
          SELECT INTO
            NEW.coordinator_v1_json
            case_subject_json(hstore(zb))
          FROM
            zaak_betrokkenen zb
          WHERE
            zb.id = NEW.coordinator;
        END IF;
      ELSE
        NEW.coordinator_v1_json := NULL;
      END IF;

      RETURN NEW;

    END;

  $$;


COMMIT;
