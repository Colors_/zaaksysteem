
BEGIN;

  ALTER TABLE file_case_document ADD COLUMN magic_string TEXT;

  UPDATE file_case_document fcd SET magic_string = bk.magic_string
  FROM zaaktype_kenmerken ztk
  JOIN bibliotheek_kenmerken bk
    ON ztk.bibliotheek_kenmerken_id = bk.id
    WHERE fcd.case_document_id = ztk.id
  ;

  ALTER TABLE file_case_document ALTER COLUMN magic_string SET NOT NULL;

COMMIT;

