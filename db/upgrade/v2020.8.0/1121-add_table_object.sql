BEGIN;
DROP TABLE IF EXISTS custom_object_version CASCADE;
DROP TABLE IF EXISTS custom_object_version_content;
DROP TABLE IF EXISTS custom_object;
--
-- Content of the object (custom fields, relationships, archiving info, etc)
--
-- Seperated from the main table for postgresql performance reasons. Showing an
-- autocomplete for instance does not need the contents for the object, only
-- the title
--
DROP TYPE IF EXISTS custom_object_version_content_archive_status;
CREATE TYPE custom_object_version_content_archive_status AS ENUM ('archived', 'to destroy', 'to preserve');
CREATE TABLE custom_object_version_content (
  id SERIAL PRIMARY KEY,
  archive_status custom_object_version_content_archive_status,
  archive_ground TEXT,
  archive_retention INTEGER,
  custom_fields JSONB
);
--
-- Version of the object
--
DROP TYPE IF EXISTS custom_object_version_status;
CREATE TYPE custom_object_version_status AS ENUM ('active', 'inactive', 'draft');
CREATE TABLE custom_object_version (
  id SERIAL PRIMARY KEY,
  uuid uuid UNIQUE NOT NULL,
  title TEXT,
  status custom_object_version_status NOT NULL DEFAULT 'active',
  version INTEGER NOT NULL,
  custom_object_version_content_id INTEGER REFERENCES custom_object_version_content(id) ON DELETE CASCADE NOT NULL,
  custom_object_type_version_id INTEGER REFERENCES custom_object_type_version(id) NOT NULL,
  date_created TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  last_modified TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  date_deleted TIMESTAMP WITHOUT TIME ZONE
);
DROP TRIGGER IF EXISTS custom_object_version_update_trigger ON custom_object_version;
CREATE TRIGGER custom_object_version_update_trigger BEFORE
UPDATE ON custom_object_version FOR EACH ROW EXECUTE PROCEDURE update_timestamps();
CREATE INDEX custom_object_version_uuid_idx ON custom_object_version(uuid);
--
  -- Version independent data
  --
  CREATE TABLE custom_object (
    id SERIAL PRIMARY KEY,
    uuid uuid UNIQUE NOT NULL,
    custom_object_version_id INTEGER REFERENCES custom_object_version(id)
  );
CREATE INDEX custom_object_uuid_idx ON custom_object(uuid);
ALTER TABLE custom_object_version
ADD
  COLUMN custom_object_id INTEGER REFERENCES custom_object(id);
COMMIT;