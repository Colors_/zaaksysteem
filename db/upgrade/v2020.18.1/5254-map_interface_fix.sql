BEGIN;

    UPDATE interface
       SET interface_config = interface_config::jsonb || '{ "map_application":"builtin" }'
     WHERE module = 'map' and interface_config::jsonb @> '{"map_application":"internal"}'::jsonb;

COMMIT;
