BEGIN;

  CREATE OR REPLACE FUNCTION is_destructable(
    IN destruction_date timestamp with time zone,
    OUT destruction boolean
  )
  LANGUAGE plpgsql
  AS $$
  BEGIN


    IF destruction_date IS NOT NULL AND destruction_date < NOW()
    THEN
      destruction := true;
    ELSE
      destruction := false;
    END IF;
  END;
  $$;




COMMIT;

