BEGIN;

  ALTER TABLE zaak
    DROP CONSTRAINT IF EXISTS zaak_behandelaar_gm_id_fkey;

  ALTER TABLE zaak
    ADD CONSTRAINT zaak_behandelaar_gm_id_fkey
    FOREIGN KEY (behandelaar_gm_id)
    REFERENCES subject(id);

  ALTER TABLE zaak
    DROP CONSTRAINT IF EXISTS zaak_coordinator_gm_id_fkey;
  ALTER TABLE zaak
    ADD CONSTRAINT zaak_coordinator_gm_id_fkey
    FOREIGN KEY (coordinator_gm_id)
    REFERENCES subject(id);

COMMIT;
