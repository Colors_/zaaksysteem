// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin.instances')
    .directive('zsInstancesCustomerForm', [
      'systemMessageService',
      'zsApi',
      'formService',
      function (systemMessageService, api, formService) {
        return {
          scope: {
            customer: '&',
            onSubmit: '&',
            onClose: '&',
          },
          template:
            '<div zs-form-template-parser data-config="instancesCustomerForm.getFormConfig()" zs-form-submit="instancesCustomerForm.handleSubmit($values)">/div>',
          controller: [
            '$scope',
            function ($scope) {
              var ctrl = this,
                config = {
                  name: 'customerForm',
                };

              config.fields = [
                {
                  name: 'template',
                  label: 'Template',
                  type: 'select',
                  data: {
                    options: _.map(
                      $scope.customer().instance.available_templates,
                      function (tplId) {
                        return {
                          value: tplId,
                          label: tplId,
                        };
                      }
                    ),
                  },
                  required: true,
                },
                {
                  name: 'shortname',
                  label: 'Short name',
                  type: 'text',
                  required: true,
                },
                {
                  name: 'allowed_instances',
                  label: 'Maximum aantal omgevingen',
                  type: 'number',
                  required: true,
                },
                {
                  name: 'allowed_diskspace',
                  label: 'Maximum diskpace (GB)',
                  type: 'number',
                  required: true,
                },
                {
                  name: 'read_only',
                  label: 'Read-only',
                  type: 'checkbox',
                  data: {
                    checkboxLabel: 'Ja',
                  },
                },
              ];

              config.actions = [
                {
                  name: 'submit',
                  type: 'submit',
                  label: 'Opslaan',
                },
              ];

              ctrl.getFormConfig = function () {
                return config;
              };

              ctrl.handleSubmit = function (values) {
                $scope.onClose();
              };

              _.each($scope.customer().instance, function (value, key) {
                var field = _.find(config.fields, { name: key });
                if (field && value !== undefined) {
                  field.value = angular.copy(value);
                }
              });

              if (
                $scope
                  .customer()
                  .instance.shortname.indexOf('betrokkene-bedrijf-') !== 0
              ) {
                config.fields = _.reject(config.fields, { name: 'shortname' });
              }
            },
          ],
          controllerAs: 'instancesCustomerForm',
        };
      },
    ]);
})();
