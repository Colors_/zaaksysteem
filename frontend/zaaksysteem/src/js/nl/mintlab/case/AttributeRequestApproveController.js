// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  'use strict';

  angular
    .module('Zaaksysteem.case')
    .controller('nl.mintlab.case.AttributeRequestApproveController', [
      '$scope',
      function ($scope) {
        $scope.editMode = false;

        $scope.enableEditMode = function () {
          $scope.editMode = true;
        };

        $scope.cancelEditMode = function () {
          $scope.editMode = false;
        };
      },
    ]);
})();
