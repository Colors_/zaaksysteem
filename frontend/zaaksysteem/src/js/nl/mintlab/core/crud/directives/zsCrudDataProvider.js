// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsCrudDataProvider', [
    '$parse',
    function ($parse) {
      return {
        require: ['zsCrudDataProvider', 'zsCrudTemplateParser'],
        controller: [
          '$scope',
          '$element',
          '$attrs',
          function ($scope, $element, $attrs) {
            var ctrl = this,
              zsCrudTemplateParser,
              getter = $parse($attrs.zsCrudDataProvider),
              loading = false;

            function getCollection() {
              var result = getter($scope);

              return result;
            }

            function resetPages() {
              var collection = getCollection(),
                l = collection ? collection.length : 0;

              $scope.numPages = Math.ceil(l / $scope.perPage) || 1;
              $scope.numRows = l;
            }

            ctrl.isLoading = function () {
              return loading;
            };

            ctrl.link = function (controllers) {
              zsCrudTemplateParser = controllers[0];

              zsCrudTemplateParser.setDataProvider(ctrl);

              function getItems() {
                var items = getCollection(),
                  start,
                  end,
                  sortBy = zsCrudTemplateParser.getSortBy(),
                  sortReversed = zsCrudTemplateParser.isSortReversed(),
                  numPerPage = zsCrudTemplateParser.getNumPerPage(),
                  currentPage = zsCrudTemplateParser.getCurrentPage();

                if (items && items.length) {
                  start = (currentPage - 1) * numPerPage;
                  end = Math.min(items.length, start + numPerPage);

                  if (sortBy) {
                    items = _.sortBy(items, sortBy);
                    if (sortReversed) {
                      items.reverse();
                    }
                  }

                  items = items.slice(start, end);
                }

                return items;
              }

              $scope.$watchCollection(getItems, function (result) {
                loading = true;

                if (result && typeof result.then === 'function') {
                  result
                    .then(function (newItems) {
                      zsCrudTemplateParser.setItems(newItems);
                    })
                    .catch(function (err) {
                      zsCrudTemplateParser.setItems([]);
                      throw err;
                    })
                    ['finally'](function () {
                      $scope.setHasLoaded();
                      loading = false;
                    });
                } else {
                  loading = false;
                  $scope.setHasLoaded();
                  zsCrudTemplateParser.setItems(result);
                }
              });

              $scope.$watchCollection(getCollection, function () {
                resetPages();
              });

              $scope.$watch('perPage', resetPages);
            };

            return ctrl;
          },
        ],
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.slice(1));
        },
      };
    },
  ]);
})();
