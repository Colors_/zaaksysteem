// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.form').directive('zsValidation', [
    '$parse',
    function ($parse) {
      return {
        scope: false,
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
          function validate(val) {
            var isValid = $parse(attrs.zsValidation)(scope, {
              $value: val,
            });

            if (isValid === undefined) {
              isValid = true;
            } else {
              isValid = !!isValid;
            }

            ngModel.$setValidity('zs-valid', isValid);

            return isValid ? val : undefined;
          }

          if (attrs.zsValidation) {
            scope.$watch(function () {
              validate(ngModel.$viewValue);
            });
          }
        },
      };
    },
  ]);
})();
