// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.collection.arrayMove', function () {
    var indexOf = window.zsFetch('nl.mintlab.utils.shims.indexOf');

    // modified from http://www.redips.net/javascript/array-move/
    return function (array, object, index) {
      // local variables
      var i,
        tmp,
        pos1 = indexOf(array, object),
        pos2 = parseInt(index, 10);

      // if positions are different and inside array
      if (
        pos1 !== pos2 &&
        0 <= pos1 &&
        pos1 <= array.length &&
        0 <= pos2 &&
        pos2 <= array.length
      ) {
        // save element from position 1
        tmp = array[pos1];
        // move element down and shift other elements up
        if (pos1 < pos2) {
          for (i = pos1; i < pos2; i++) {
            array[i] = array[i + 1];
          }
        }
        // move element up and shift other elements down
        else {
          for (i = pos1; i > pos2; i--) {
            array[i] = array[i - 1];
          }
        }
        // put element from position 1 to destination
        array[pos2] = tmp;
      }
    };
  });
})();
