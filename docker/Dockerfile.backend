FROM registry.gitlab.com/zaaksysteem/zaaksysteem-perl:496a1b0 as production

RUN apt-get update \
  && apt-get install -y --no-install-recommends sudo \
  && apt-get autoremove --purge -yqq\
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

COPY docker/inc/sudo/* /etc/sudoers.d/
RUN chmod 0400 /etc/sudoers.d/*

COPY script  /opt/zaaksysteem/script
COPY share   /opt/zaaksysteem/share
COPY bin     /opt/zaaksysteem/bin
COPY dev-bin /opt/zaaksysteem/dev-bin
COPY xt      /opt/zaaksysteem/xt
COPY t       /opt/zaaksysteem/t
COPY root    /opt/zaaksysteem/root
COPY lib     /opt/zaaksysteem/lib

COPY root/csp.json /etc/zaaksysteem/csp.json

WORKDIR /opt/zaaksysteem
RUN perl "-MExtUtils::Manifest=mkmanifest" -e mkmanifest; \
    egrep "\.(pod|pm)" MANIFEST | egrep -v '^(MANIFEST.pod)' > MANIFEST.pod

RUN date > /build-timestamp

USER zaaksysteem

ENV ZAAKSYSTEEM_HOME /opt/zaaksysteem
ENV PERL5LIB $ZAAKSYSTEEM_HOME/lib:$PERL5LIB
# We do this on container start time
#EXPOSE 9083
COPY docker/inc/backend/entrypoint.sh /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]
CMD ["perl", "/opt/zaaksysteem/script/zaaksysteem_fastcgi.pl", "-l", ":9083"]

FROM production as development

USER root
# Some things break more easily than others
RUN cpanm -n MooseX::Daemonize \
    && cpanm -n Net::Server Starman \
    && cpanm DBIx::Class::Schema::Loader Catalyst::Helper
RUN apt-get update \
    && apt-get install -y --no-install-recommends libxml2-utils vim-tiny \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

USER zaaksysteem
